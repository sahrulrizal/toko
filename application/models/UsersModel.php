<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

  public $t = 'user';
  public $id_user = 0;
  private $level = 0;

  public function __construct()
  {
    parent::__construct();
      //Do your magic here
    $this->level = $this->session->userdata('level');
  }

  public function dtUsers()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->t;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'outlet_name', 'nama', 'username','no_telp');
        // Set searchable column fields
        $CI->dt->column_search = array('outlet_name', 'nama', 'username','no_telp');
        // Set select column fields
        $CI->dt->select = $this->t . '.*';
        // Set default order
        $CI->dt->order = array($this->t . '.id' => 'desc');

        $data = $row = array();

        $condition = 
        [
         ['where',$this->t.'.level','2'],
        ];  

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                $dt->outlet_name,
                $dt->nama,
                $dt->username,
                $dt->no_telp,
                '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getUsers('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-edit"></i></a>',
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

  public function getUsers($id='',$q='',$obj='')
  { 

    if ($id != '') {
      $obj = ['id' => $id];
    }


    if ($obj != 0) {
      $q = $this->db->get_where($this->t,$obj);
    }else if ($q != '') {
      $q = $this->db->query($q);
    }else{
      $q = $this->db->get_where($this->t);
    }
    
    return $q;
  }

  public function inUsers($obj='')
  {
    $log = '';

    if ($obj != '') {
      $q = $this->db->insert($this->t, $obj);  
    }

    $log = [
      'response' => $q,
      'request' => $obj,
      'date' => date('Y-m-d H:i:s'),
    ];  

    return $log;
  }

  public function upUsers($obj='',$id='')
  {
    $log = '';
    $based_on = '';

    if ($id != '') {
      $based_on = ['id' => $id];
    }

    $q = $this->db->update($this->t, $obj,$based_on);

    $log = [
      'response' => $q,
      'request' => $obj,
      'msg' => 'Sukses ubah Profile',
      'date' => date('Y-m-d H:i:s'),
    ];  

    return $log;
  }

  public function deUsers($id='')
  {
    $log = '';

    if ($id != '') {
      $based_on = ['id' => $id];
    }


    $this->db->delete($this->t,$based_on);

    $log = [
      'response' => $q,
      'request' => $obj,
      'date' => date('Y-m-d H:i:s'),
    ];  

    return $log;
  }

  // OPTIONAL

  public function cekStatus($v='')
  {
    switch ($v) {
      case '1':
        $val = 'Admin';
      break;
      case '2':
        $val =  'Pimpinan';
      break;
      case '3':
        $val =  'Super Admin';
      break;
      
      default:
       $val =  'Tidak Diketahui';
      break;
    }

    return $val;

  }

  public function cekLevelBoleh($v='')
  {
    if ($v == '') {
      $v = $this->level;
    }
    
    switch ($v) {
      case '1':
        $val = 'readonly';
      break;
      case '2':
        $val =  '';
      break;
      case '3':
        $val =  '';
      break;
      
      default:
       $val =  'disabled';
      break;
    }

    return $val;
  }


}

/* End of file UsersModel.php */
/* Location: ./application/models/UsersModel.php */