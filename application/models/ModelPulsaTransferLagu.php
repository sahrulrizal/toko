<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPulsaTransferLagu extends CI_Model
{

    public $tabel = 'pulsa_transfer_lagu';
    public $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Model File');
    }

    public function dtPulsaTransferLagu()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'keterangan', 'harga');
        // Set searchable column fields
        $CI->dt->column_search = array('keterangan', 'harga');
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->keterangan,
                $dt->harga,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPTLID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-edit"></i></a> ' .
                '<a href="javascript:;" onclick="prosesDePTL('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPTL($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE keterangan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPTL()
    {
        // Definisi
        $object = [
            'keterangan' => $this->input->post('keterangan'),
            'harga' => $this->input->post('harga'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah service',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPTL($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'keterangan' => $this->input->post('e_keterangan'),
            'harga' => $this->input->post('e_harga'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);

        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePTL($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }
        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}