<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DiskonModel extends CI_Model 	{

	public $rpb = 'riwayat_penjualan_barang';
	public $rtb = 'riwayat_transaksi_barang';
    public $id_user = 0;

	public function dtDiskon()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->rpb;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'created_date', 'kode_struk');
        // Set searchable column fields
        $CI->dt->column_search = array('created_date', 'kode_struk');
        // Set select column fields
        $CI->dt->select = $this->rpb . '.*,rtb.diskon';
        // Set default order
        $CI->dt->order = array($this->rpb . '.id' => 'desc');

        $data = $row = array();
            
        //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->rpb.'.id_user',$id_user];
        }else{
          $user = ['where',$this->rpb.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->rpb.'.created_date',$bulan,'right'];
        }else{
          $bulan = ['like',$this->rpb.'.created_date',date('Y-m'),'right'];
        }


         $condition = 
        [
         ['join',$this->rtb.' rtb',$this->rpb.'.id = rtb.id_pj_barang','inner'],
         ['where_not_in','diskon',[0,'null']],
         $user,
         $bulan
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $ks = "'".$dt->kode_struk."'";
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->created_date,
                $dt->kode_struk,
                $dt->diskon,
                // $dt->nama_barang,
                // $dt->jumlah,
                // 'Rp '.number_format($dt->harga_jual,0,",","."),
                // 'Rp '.number_format($dt->diskon,0,",","."),
                // 'Rp '.number_format($dt->total_all,0,",","."),
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                '<a href="javascript:;" data-toggle="modal" data-target="#view" onclick="getPenjualanID('.$ks.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-eye"></i></a>',
                // '<a href="javascript:;" onclick="prosesDePenjualan('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }	

}

/* End of file DiskonModel.php */
/* Location: ./application/models/DiskonModel.php */