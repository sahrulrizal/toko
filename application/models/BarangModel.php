<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BarangModel extends CI_Model {

    public $tabel = 'barang';

	 public function __construct()
    {
        parent::__construct();
    }

    public function dtBarang()
    {
          // Definisi
        $condition = '';

        $CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'nama_barang','kode','harga_beli','harga_jual','stok','tanggal');
        // Set searchable column fields
        $CI->dt->column_search = array('nama_barang','kode','harga_beli','harga_jual','stok','tanggal');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
        

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $data[] = array(
                $i,
                "<a href='".site_url('back/barang/editBarang?id='.$dt->id)."' >".$dt->nama_barang."</a>",
                $dt->kode,
                // $dt->kategori,
                // $dt->supplier,
                'Rp '.number_format($dt->harga_beli,0,",","."),
                'Rp '.number_format($dt->harga_jual,0,",","."),
                $dt->stok,
                $dt->tanggal,
                 '<a href="javascript:;" onclick="getBarangID('.$dt->id.')" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs item_edit"><i class="fa fa-edit"></i></a> ' .
                '<a href="javascript:;" onclick="prosesDeBarang('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa  fa-trash"></i></a> '

            );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    public function getBarang($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE nama_barang like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInBarang()
    {

        // Definisi
        $idSupplier = $this->input->post('nama_supplier');
        $idKategori = $this->input->post('kategori');

        # MENGAMBIL DARI TABEL SUPPLIER#
        $supplier = $this->db->get_where('supplier',['id' => $idSupplier]);
        
        if ($supplier->num_rows() > 0) {
           $supplier = $supplier->row();
           $idSupplier = $supplier->id;
        }else{
            $this->db->insert('supplier', [
                'supplier' => $idSupplier,
            ]);
            $idSupplier = $this->db->insert_id();
        }

        # MENGAMBIL DARI TABEL KATEGORI#
        $kategori = $this->db->get_where('kategori_barang',['id' => $idKategori]);
        
        if ($kategori->num_rows() > 0) {
           $kategori = $kategori->row();
           $idKategori = $kategori->id;
        }else{
            $this->db->insert('kategori_barang', [
                'kategori' => $idKategori,
            ]);
            $idKategori = $this->db->insert_id();
        }

        $object = [
            'supplier' => $idSupplier,
            'kode' => $this->input->post('kode_barang'),
            'kategori' => $idKategori,
            'nama_barang' => $this->input->post('nama_barang'),
            'harga_jual' => $this->input->post('harga_jual'),
            'harga_beli' => $this->input->post('harga_beli'),
            'stok' => $this->input->post('jumlah_stok'),
            'tanggal' => date('Y-m-d H:i:s'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah barang '.$this->input->post('nama_barang'),
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpBarang($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        // Definisi
        $idSupplier = $this->input->post('e_nama_supplier');
        $idKategori = $this->input->post('e_kategori');

        # MENGAMBIL DARI TABEL SUPPLIER#
        $supplier = $this->db->get_where('supplier',['id' => $idSupplier]);
        
        if ($supplier->num_rows() > 0) {
           $supplier = $supplier->row();
           $idSupplier = $supplier->id;
        }else{
            $this->db->insert('supplier', [
                'supplier' => $idSupplier,
            ]);
            $idSupplier = $this->db->insert_id();
        }

        # MENGAMBIL DARI TABEL KATEGORI#
        $kategori = $this->db->get_where('kategori_barang',['id' => $idKategori]);
        
        if ($kategori->num_rows() > 0) {
           $kategori = $kategori->row();
           $idKategori = $kategori->id;
        }else{
            $this->db->insert('kategori_barang', [
                'kategori' => $idKategori,
            ]);
            $idKategori = $this->db->insert_id();
        }

        $object = [
            'supplier' => $idSupplier,
            'kode' => $this->input->post('e_kode_barang'),
            'kategori' => $idKategori,
            'nama_barang' => $this->input->post('e_nama_barang'),
            'harga_jual' => $this->input->post('e_harga_jual'),
            'harga_beli' => $this->input->post('e_harga_beli'),
            'stok' => $this->input->post('e_jumlah_stok'),
            'tanggal' => date('Y-m-d H:i:s'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data barang',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDeBarang($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}