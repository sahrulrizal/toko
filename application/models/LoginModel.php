<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {

    public $username;
    public $password;
    public $tbl = 'user';

	 public function __construct()
    {
        parent::__construct();
    }

    public function login($username=null,$password=null)
    {
       $this->username = $username;
       $this->password = $password;
    }

    public function prosesKeLogin()
    {
        $to = '';

        $qLogin = $this->db->get_where($this->tbl,[
        'username' => $this->username,
        'password' => md5($this->password)
        ]);

        if ($qLogin->num_rows() > 0 ) {
            
            $q = $qLogin->row();
            
            $session = [
                'id' => $q->id,
                'level' => $q->level
            ];

            $this->session->set_userdata($session);

            if ($q->level == 3) {
                $this->session->set_flashdata('berhasil','Berhasil login sebagai Outlet');
                $to = 'back/profile';
            }elseif ($q->level == 2) {
                $this->session->set_flashdata('berhasil','Berhasil login sebagai Pimpinan');
                $to = 'back/profile';
            }elseif ($q->level == 1) {
                $this->session->set_flashdata('berhasil','Berhasil login');
                $to = 'back/profile';
            }else{
                 $this->session->set_flashdata('info','Gagal Login, dikarenakan level pengguna tidak dikenali');
                 $to = '/';
            }

        }else{
            $this->session->set_flashdata('gagal','Gagal Login, harap periksa kembali Username dan Password Anda.');
            $to = '/';
        }

        redirect($to);
    }   
}