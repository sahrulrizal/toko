<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PIUModel extends CI_Model
{

    public $tabel = 'piu_deposit1';
    public $t_d2 = 'piu_deposit2';
    public $t_matauli = 'piu_matauli';
    public $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Model File');
    }

    //  ~DEPOSIT 1

    public function dtPIUD1()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'keterangan', 'harga');
        // Set searchable column fields
        $CI->dt->column_search = array('keterangan', 'harga');
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->keterangan,
                $dt->harga,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPIUD1ID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-edit"></i></a> ' .
                '<a href="javascript:;" onclick="prosesDePIUD1('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPIUD1($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE keterangan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPIUD1()
    {
        // Definisi
        $object = [
            'keterangan' => $this->input->post('keterangan'),
            'harga' => $this->input->post('harga'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah PIUD1',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPIUD1($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'keterangan' => $this->input->post('e_keterangan'),
            'harga' => $this->input->post('e_harga'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePIUD1($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }

     // ~DEPOSIT 2

      public function dtPIUD2()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->t_d2;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'keterangan', 'harga');
        // Set searchable column fields
        $CI->dt->column_search = array('keterangan', 'harga');
        // Set select column fields
        $CI->dt->select = $this->t_d2 . '.*';
        // Set default order
        $CI->dt->order = array($this->t_d2 . '.id' => 'desc');

        $data = $row = array();

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->keterangan,
                $dt->harga,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPIUD2ID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-edit"></i></a> ' .
                '<a href="javascript:;" onclick="prosesDePIUD2('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPIUD2($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->t_d2." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->t_d2.' WHERE keterangan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->t_d2,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPIUD2()
    {
        // Definisi
        $object = [
            'keterangan' => $this->input->post('keterangan'),
            'harga' => $this->input->post('harga'),
        ];

        $q = $this->db->insert($this->t_d2, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->t_d2,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->t_d2)->first_row(),
            'last_row' => $this->db->get($this->t_d2)->last_row(),
            'previous_row' => $this->db->get($this->t_d2)->previous_row(),
            'next_row' => $this->db->get($this->t_d2)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah PIUD1 Bulk',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPIUD2($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'keterangan' => $this->input->post('e_keterangan'),
            'harga' => $this->input->post('e_harga'),
        ];

        $q = $this->db->update($this->t_d2, $object, ['id' => $id]);
        $response = $this->db->get_where($this->t_d2,$id);
        $result = array(
            'first_row' => $this->db->get($this->t_d2)->first_row(),
            'last_row' => $this->db->get($this->t_d2)->last_row(),
            'previous_row' => $this->db->get($this->t_d2)->previous_row(),
            'next_row' => $this->db->get($this->t_d2)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePIUD2($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->t_d2,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }

     // ~MATAULI

      public function dtPIUMatauli()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->t_matauli;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'keterangan', 'harga');
        // Set searchable column fields
        $CI->dt->column_search = array('keterangan', 'harga');
        // Set select column fields
        $CI->dt->select = $this->t_matauli . '.*';
        // Set default order
        $CI->dt->order = array($this->t_matauli . '.id' => 'desc');

        $data = $row = array();

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->keterangan,
                $dt->harga,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPIUMatauliID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-edit"></i></a> ' .
                '<a href="javascript:;" onclick="prosesDePIUMatauli('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPIUMatauli($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->t_matauli." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->t_matauli.' WHERE keterangan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->t_matauli,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPIUMatauli()
    {
        // Definisi
        $object = [
            'keterangan' => $this->input->post('keterangan'),
            'harga' => $this->input->post('harga'),
        ];

        $q = $this->db->insert($this->t_matauli, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->t_matauli,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->t_matauli)->first_row(),
            'last_row' => $this->db->get($this->t_matauli)->last_row(),
            'previous_row' => $this->db->get($this->t_matauli)->previous_row(),
            'next_row' => $this->db->get($this->t_matauli)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah PIUD1 Bulk',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPIUMatauli($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'keterangan' => $this->input->post('e_keterangan'),
            'harga' => $this->input->post('e_harga'),
        ];

        $q = $this->db->update($this->t_matauli, $object, ['id' => $id]);
        $response = $this->db->get_where($this->t_matauli,$id);
        $result = array(
            'first_row' => $this->db->get($this->t_matauli)->first_row(),
            'last_row' => $this->db->get($this->t_matauli)->last_row(),
            'previous_row' => $this->db->get($this->t_matauli)->previous_row(),
            'next_row' => $this->db->get($this->t_matauli)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePIUMatauli($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->t_matauli,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}