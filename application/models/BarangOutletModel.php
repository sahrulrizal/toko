<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BarangOutletModel extends CI_Model {

    public $tabel = 'barang_outlet';
    public $id_user = 1;

	 public function __construct()
    {
      parent::__construct();
    }

    public function dtBarang()
    {
          // Definisi
        $condition = '';

        $CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'b.nama_barang','harga_satuan','harga_jual','stok','tanggal_beli');
        // Set searchable column fields
        $CI->dt->column_search = array('b.nama_barang','harga_satuan','harga_jual','stok','tanggal_beli');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*,b.nama_barang';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
        
        $condition = 
        [
         ['join','barang b','b.id = '.$this->tabel.'.id_barang','inner'],
         ['where',$this->tabel.'.id_user',$this->id_user],
        ];    

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $data[] = array(
                $i,
                $dt->nama_barang,
                $dt->stok,
                'Rp '.number_format($dt->harga_satuan,0,",","."),
                'Rp '.number_format($dt->harga_jual,0,",","."),
                $dt->status == 1 ? 'Aktif' : 'Non Aktif',
                $dt->tanggal_beli,
                 '<a href="javascript:void(0);" onclick="getBarangID('.$dt->id_barang.')" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs item_edit"><i class="fa fa-edit"></i></a> ',
            );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    public function getBarang($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT ba.*,b.nama_barang FROM ".$this->tabel." ba INNER JOIN barang b ON b.id = ba.id_barang WHERE ba.id_barang=".$id." AND id_user=".$this->id_user,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT b.id,ba.harga_jual,b.nama_barang FROM ".$this->tabel." ba INNER JOIN barang b ON b.id = ba.id_barang WHERE b.nama_barang like '%".$value."%' AND id_user=".$this->id_user,
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT b.id,bo.harga_jual,bo.harga_satuan,b.nama_barang FROM ".$this->tabel." bo INNER JOIN barang b ON bo.id_barang = b.id",
                ));
            } 

        }

        return $q;
    }

    public function prosesInBarang()
    {

        // Definisi
        $idSupplier = $this->input->post('nama_supplier');
        $idKategori = $this->input->post('kategori');

        # MENGAMBIL DARI TABEL SUPPLIER#
        $supplier = $this->db->get_where('supplier',['id' => $idSupplier]);
        
        if ($supplier->num_rows() > 0) {
           $supplier = $supplier->row();
           $idSupplier = $supplier->id;
        }else{
            $this->db->insert('supplier', [
                'supplier' => $idSupplier,
            ]);
            $idSupplier = $this->db->insert_id();
        }

        # MENGAMBIL DARI TABEL KATEGORI#
        $kategori = $this->db->get_where('kategori_barang',['id' => $idKategori]);
        
        if ($kategori->num_rows() > 0) {
           $kategori = $kategori->row();
           $idKategori = $kategori->id;
        }else{
            $this->db->insert('kategori_barang', [
                'kategori' => $idKategori,
            ]);
            $idKategori = $this->db->insert_id();
        }

        $object = [
            'supplier' => $idSupplier,
            'kode' => $this->input->post('kode_barang'),
            'kategori' => $idKategori,
            'nama_barang' => $this->input->post('nama_barang'),
            'harga_jual' => $this->input->post('harga_jual'),
            'harga_beli' => $this->input->post('harga_beli'),
            'stok' => $this->input->post('jumlah_stok'),
            'id_user' => $this->id_user,
            'tanggal' => date('Y-m-d H:i:s'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah barang '.$this->input->post('nama_barang'),
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpBarang($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        // // Definisi
        // $idSupplier = $this->input->post('e_nama_supplier');
        // $idKategori = $this->input->post('e_kategori');

        // # MENGAMBIL DARI TABEL SUPPLIER#
        // $supplier = $this->db->get_where('supplier',['id' => $idSupplier]);
        
        // if ($supplier->num_rows() > 0) {
        //    $supplier = $supplier->row();
        //    $idSupplier = $supplier->id;
        // }else{
        //     $this->db->insert('supplier', [
        //         'supplier' => $idSupplier,
        //     ]);
        //     $idSupplier = $this->db->insert_id();
        // }

        // # MENGAMBIL DARI TABEL KATEGORI#
        // $kategori = $this->db->get_where('kategori_barang',['id' => $idKategori]);
        
        // if ($kategori->num_rows() > 0) {
        //    $kategori = $kategori->row();
        //    $idKategori = $kategori->id;
        // }else{
        //     $this->db->insert('kategori_barang', [
        //         'kategori' => $idKategori,
        //     ]);
        //     $idKategori = $this->db->insert_id();
        // }

        $object = [
            'harga_jual' => $this->input->post('e_harga_jual'),
            'status' => $this->input->post('e_status')
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data barang',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDeBarang($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}