<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClusterModel extends CI_Model {

	 public function __construct()
    {
        parent::__construct();
    	$this->db2 = $this->load->database('db2', TRUE);
    }

    private $tabel = 'cluster';
    private $db = 'bonitanew';

    public function getCluster($tgl='')
    {
        // Definisi
        $condition = '';

 		$CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'cluster');
        // Set searchable column fields
        $CI->dt->column_search = array('cluster');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
        

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $data[] = array(
            	$i,
                "<a href='".site_url('cluster/editCluster?id='.$dt->id)."' >".$dt->cluster."</a>",
            );
            // "<a href='javascript:void(0)' onclick='detail(".$dt->id.")' data-toggle='modal' data-target='#detail'>".$dt->cluster."</a>",
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    public function getClusterID($id='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id = ".$id,
        ));   

        return $q;
    }


    // PROSES

    public function prosesAddToCluster()
    {
        $arr = array();
        $areaCluster = array();

        $cluster = $this->input->get('cluster');
        $municipio = $this->input->get('municipio');
        $posto = $this->input->get('posto');
        $suco = $this->input->get('suco');
        $aldeia = $this->input->get('aldeia');

        $cluster = $this->db2->insert('cluster', array('cluster' => $cluster));
        $cluster_id = $this->db2->insert_id();

        if ($cluster) {
            foreach ($aldeia as $key => $value) {
                $q = $this->db2->query("SELECT 
                    m.id AS mId,
                    pa.id AS paId,
                    s.id AS sId,
                    a.id AS aId,
                    m.name,
                    pa.name,
                    s.name,
                    a.name
                    FROM
                    municipio m INNER JOIN posto_adm pa ON m.id = pa.municipio_id
                    INNER JOIN
                    suco s ON pa.id = s.posto_adm_id
                    INNER JOIN
                    aldeia a ON s.id = a.suco_id
                    WHERE a.id = ".$aldeia[$key])->row();

                $areaCluster = array(
                    'municipio' => $q->mId,
                    'posto_adm' => $q->paId,
                    'suco' => $q->sId,
                    'aldeia' => $q->aId,
                    'cluster_id' => $cluster_id,
                    'created_by' => 1,
                    'created_date' => date('Y-m-d H:i:s')
                );

                array_push($arr, $areaCluster);
            }

            $this->db2->insert_batch('areacluster', $arr);

        }else{

        }

        echo json_encode($arr);
    }
}