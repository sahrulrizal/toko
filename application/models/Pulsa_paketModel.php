<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pulsa_paketModel extends CI_Model
{

    public $tabel = 'riwayat_penjualan_pulsa';

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Model File');
    }

    public function dtPTDL()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'tanggal_transaksi', 'ptf.keterangan','nomor_tujuan','ptf.harga');
        // Set searchable column fields
        $CI->dt->column_search = array('tanggal_transaksi', 'ptf.keterangan','nomor_tujuan','ptf.harga' );
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*,ptf.keterangan,ptf.harga';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();

        //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',date('Y-m'),'right'];
        }

        $condition = 
        [
         ['join','pulsa_transfer_lagu ptf','ptf.id = '.$this->tabel.'.id_kategori_pulsa','inner'],
         ['where','id_jenis_transaksi_pulsa',6],
         $user,
         $bulan
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/ptf/editBarang?id=' . $dt->id) . "' >" . $dt->nama_ptf . "</a>",
                $dt->tanggal_transaksi,
                $dt->keterangan,
                $dt->nomor_tujuan,
                'Rp '.number_format($dt->harga,0,",","."),
                // "<a href='" . site_url('back/ptf/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                // '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPTDLID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa fa-edit"></i></a>' .
                // '<a href="javascript:;" onclick="prosesDePTDL('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPTDL($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE nomor_tujuan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPTDL()
    {
        // Definisi
        $object = [
            'id_jenis_transaksi_pulsa' => 6,
            'id_kategori_pulsa' => $this->input->post('ppdl'),
            'tanggal_transaksi' => date('Y-m-d H:i:s'),
             'id_user' => $this->id_user,
            'nomor_tujuan' => $this->input->post('nomor_tujuan'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah PTDL',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPTDL($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'id_kategori_pulsa' => $this->input->post('ppdl'),
            'tanggal_transaksi' => date('Y-m-d H:i:s'),
            'nomor_tujuan' => $this->input->post('nomor_tujuan'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePTDL($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }

     // ~ PULSA DEPOSIT

      public function dtPD()
    {

        $id = $this->input->get('id');
        if ($id == 1) {
            $tbl_pilih = 'pulsa_deposit';
        }else if ($id == 2) {
            $tbl_pilih = 'pulsa_deposit_bulk';
        }

        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'tanggal_transaksi','jp.jenis', 'p.keterangan','nomor_tujuan','p.harga');
        // Set searchable column fields
        $CI->dt->column_search = array('tanggal_transaksi','jp.jenis', 'p.keterangan','nomor_tujuan','p.harga' );
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*,jp.jenis,p.keterangan,p.harga';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();


        //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',date('Y-m'),'right'];
        }

        $condition = 
        [
         ['join','jenis_transaksi_pulsa jp','jp.id = '.$this->tabel.'.id_jenis_transaksi_pulsa','inner'],
         ['join',$tbl_pilih.' p','p.id = '.$this->tabel.'.id_kategori_pulsa','inner'],
         ['where','id_jenis_transaksi_pulsa', $id],
          $user,
          $bulan
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/pd/editBarang?id=' . $dt->id) . "' >" . $dt->nama_ptf . "</a>",
                $dt->tanggal_transaksi,
                $dt->jenis,
                $dt->keterangan,
                $dt->nomor_tujuan,
                'Rp '.number_format($dt->harga,0,",","."),
                // "<a href='" . site_url('back/ptf/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                // '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPDID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa fa-edit"></i></a>' .
                // '<a href="javascript:;" onclick="prosesDePD('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPD($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE nomor_tujuan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPD()
    {
        // Definisi
        $object = [
            'id_jenis_transaksi_pulsa' => $this->input->post('jpd'),
            'id_kategori_pulsa' => $this->input->post('pp'),
            'tanggal_transaksi' => date('Y-m-d H:i:s'),
            'id_user' => $this->id_user,
            'nomor_tujuan' => $this->input->post('nomor_tujuan'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah PD',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPD($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'id_kategori_pulsa' => $this->input->post('ppdl'),
            'tanggal_transaksi' => date('Y-m-d H:i:s'),
            'nomor_tujuan' => $this->input->post('nomor_tujuan'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePD($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }

      // ~ PAKET ISI ULANG

      public function dtPIU()
    {

        $id = $this->input->get('id');
        if ($id == 3) {
            $tbl_pilih = 'piu_deposit1';
        }else if ($id == 4) {
            $tbl_pilih = 'piu_deposit2';
        }else if ($id == 5) {
            $tbl_pilih = 'piu_matauli';
        }

        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'tanggal_transaksi','jp.jenis', 'p.keterangan','nomor_tujuan','p.harga');
        // Set searchable column fields
        $CI->dt->column_search = array('tanggal_transaksi','jp.jenis', 'p.keterangan','nomor_tujuan','p.harga' );
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*,jp.jenis,p.keterangan,p.harga';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();
        
         //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',date('Y-m'),'right'];
        }

        $condition = 
        [
         ['join','jenis_transaksi_pulsa jp','jp.id = '.$this->tabel.'.id_jenis_transaksi_pulsa','inner'],
         ['join',$tbl_pilih.' p','p.id = '.$this->tabel.'.id_kategori_pulsa','inner'],
         ['where','id_jenis_transaksi_pulsa', $id],
          $user,
          $bulan
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/pd/editBarang?id=' . $dt->id) . "' >" . $dt->nama_ptf . "</a>",
                $dt->tanggal_transaksi,
                $dt->jenis,
                $dt->keterangan,
                $dt->nomor_tujuan,
                'Rp '.number_format($dt->harga,0,",","."),
                // "<a href='" . site_url('back/ptf/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                // '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPIUID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa fa-edit"></i></a>' .
                // '<a href="javascript:;" onclick="prosesDePIU('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPIU($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE nomor_tujuan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPIU()
    {
        // Definisi
        $object = [
            'id_jenis_transaksi_pulsa' => $this->input->post('jpd'),
            'id_kategori_pulsa' => $this->input->post('pp'),
            'tanggal_transaksi' => date('Y-m-d H:i:s'),
             'id_user' => $this->id_user,
            'nomor_tujuan' => $this->input->post('nomor_tujuan'),
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah PIU',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPIU($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'id_kategori_pulsa' => $this->input->post('ppdl'),
            'tanggal_transaksi' => date('Y-m-d H:i:s'),
            'nomor_tujuan' => $this->input->post('nomor_tujuan'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePIU($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}