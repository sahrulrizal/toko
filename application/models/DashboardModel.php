<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardModel extends CI_Model {

	public $id_user = 0;
	public $dapat;
	public $tahun;
	public function __construct()
    {
        parent::__construct();
    }

    public function pendapatan($id_user='',$tanggal='')
    {
    	$log = [];
    	if ($id_user == '') {
    			$id_user = $this->id_user;
    	}	

    	if ($tanggal == '') {
    		$tanggal = date('Y-m-');
    	}	

    	$qPenjualan = $this->db->query("SELECT SUM(total_all) AS total_penjualan FROM toko.riwayat_transaksi_barang WHERE id_user=$id_user AND id_jenis_transaksi=2 AND tanggal_transaksi LIKE '".$tanggal."%'")->row()->total_penjualan;

    	$qPembelian = $this->db->query("SELECT SUM(total_harga) AS total_pembelian FROM toko.riwayat_transaksi_barang WHERE id_user=$id_user AND id_jenis_transaksi=1 AND tanggal_transaksi LIKE '$tanggal%'")->row()->total_pembelian;

    	$qRetur = $this->db->query("SELECT SUM(total_harga) AS total_retur FROM toko.riwayat_transaksi_barang WHERE id_user=$id_user AND id_jenis_transaksi=3 AND tanggal_transaksi LIKE '$tanggal%'")->row()->total_retur;

    	$qDiskon = $this->db->query("SELECT SUM(diskon) AS total_diskon FROM toko.riwayat_transaksi_barang WHERE id_user=$id_user AND id_jenis_transaksi=2 AND tanggal_transaksi LIKE '$tanggal%'")->row()->total_diskon;

    	$qService = $this->db->query("SELECT SUM(biaya) as total FROM toko.service_hp WHERE id_user=$id_user AND tanggal_service LIKE '$tanggal%'")->row()->total;

    	$qPengeluaran = $this->db->query("SELECT SUM(jumlah_pengeluaran) AS total_pengeluaran FROM toko.riwayat_pengeluaran WHERE id_user=$id_user AND tgl_pengeluaran LIKE  '$tanggal%'")->row()->total_pengeluaran;

    	$qPTL = $this->db->query("SELECT SUM(harga) AS transfer_lagu FROM toko.riwayat_penjualan_pulsa AS rpp INNER JOIN toko.jenis_transaksi_pulsa AS jtp ON rpp.id_jenis_transaksi_pulsa=jtp.id INNER JOIN toko.pulsa_transfer_lagu AS ptl ON rpp.id_kategori_pulsa=ptl.id WHERE rpp.tanggal_transaksi LIKE '$tanggal%' AND rpp.id_user=$id_user AND rpp.id_jenis_transaksi_pulsa=6")->row()->transfer_lagu;

    	$qDeposit = $this->db->query("SELECT SUM(harga) AS deposit FROM toko.riwayat_penjualan_pulsa AS rpp INNER JOIN toko.jenis_transaksi_pulsa AS jtp ON rpp.id_jenis_transaksi_pulsa=jtp.id INNER JOIN toko.pulsa_deposit AS pd ON rpp.id_kategori_pulsa=pd.id WHERE rpp.tanggal_transaksi LIKE '$tanggal%' AND rpp.id_user=$id_user AND rpp.id_jenis_transaksi_pulsa=1")->row()->deposit;

    	$qDepositBulk = $this->db->query("SELECT SUM(harga) AS deposit_bulk FROM toko.riwayat_penjualan_pulsa AS rpp INNER JOIN toko.jenis_transaksi_pulsa AS jtp ON rpp.id_jenis_transaksi_pulsa=jtp.id INNER JOIN toko.pulsa_deposit_bulk AS pdb ON rpp.id_kategori_pulsa=pdb.id WHERE rpp.tanggal_transaksi LIKE '$tanggal%' AND rpp.id_user=$id_user AND rpp.id_jenis_transaksi_pulsa=2")->row()->deposit_bulk;

    	$qPiuDeposit1 = $this->db->query("SELECT SUM(harga) AS total FROM toko.riwayat_penjualan_pulsa AS rpp INNER JOIN toko.jenis_transaksi_pulsa AS jtp ON rpp.id_jenis_transaksi_pulsa=jtp.id INNER JOIN toko.piu_deposit1 AS pd1 ON rpp.id_kategori_pulsa=pd1.id WHERE rpp.tanggal_transaksi LIKE '$tanggal%' AND rpp.id_user=$id_user AND rpp.id_jenis_transaksi_pulsa=3")->row()->total;

    	$qPiuDeposit2 = $this->db->query("SELECT SUM(harga) AS total FROM toko.riwayat_penjualan_pulsa AS rpp INNER JOIN toko.jenis_transaksi_pulsa AS jtp ON rpp.id_jenis_transaksi_pulsa=jtp.id INNER JOIN toko.piu_deposit1 AS pd1 ON rpp.id_kategori_pulsa=pd1.id WHERE rpp.tanggal_transaksi LIKE '$tanggal%' AND rpp.id_user=$id_user AND rpp.id_jenis_transaksi_pulsa=4")->row()->total;

    	$qPiuMatauli = $this->db->query("SELECT SUM(harga) AS total FROM toko.riwayat_penjualan_pulsa AS rpp INNER JOIN toko.jenis_transaksi_pulsa AS jtp ON rpp.id_jenis_transaksi_pulsa=jtp.id INNER JOIN toko.piu_deposit1 AS pd1 ON rpp.id_kategori_pulsa=pd1.id WHERE rpp.tanggal_transaksi LIKE '$tanggal%' AND rpp.id_user=$id_user AND rpp.id_jenis_transaksi_pulsa=5")->row()->total;


    	$uangMasuk = $qPenjualan+$qService+$qPTL+$qDeposit+$qDepositBulk+$qPiuDeposit1+$qPiuDeposit2+$qPiuMatauli;
    	$uangKeluar = $qPengeluaran+$qDiskon+$qRetur+$qPembelian;

    	$vdeposit = $qDeposit+$qDepositBulk;
    	$vPIU = $qPiuDeposit1+$qPiuDeposit2+$qPiuMatauli;

    	$log = 
    		 [
    			'penjualan' => 'Rp '.number_format($qPenjualan,0,",","."),
    			'pembelian' => 'Rp '.number_format($qPembelian,0,",","."),
    			'retur' => 'Rp '.number_format($qRetur,0,",","."),
    			'diskon' => 'Rp '.number_format($qDiskon,0,",","."),
    			'service' => 'Rp '.number_format($qService,0,",","."),
    			'pengeluaran' => 'Rp '.number_format($qPengeluaran,0,",","."),
    			'ptl' => 'Rp '.number_format($qPTL,0,",","."),
    			'deposit' => 'Rp '.number_format($qDeposit,0,",","."),
    			'depositBulk' => 'Rp '.number_format($qDepositBulk,0,",","."),
    			'piuD1' => 'Rp '.number_format($qPiuDeposit1,0,",","."),
    			'piuD2' => 'Rp '.number_format($qPiuDeposit2,0,",","."),
    			'qPiuMatauli' => 'Rp '.number_format($qPiuMatauli,0,",","."),
    			'vdeposit' => 'Rp '.number_format($vdeposit,0,",","."),
    			'vPIU' => 'Rp '.number_format($vPIU,0,",","."),
    			'uangMasuk' => 'Rp '.number_format($uangMasuk,0,",","."),
    			'uangKeluar' =>'Rp '.number_format($uangKeluar,0,",","."),
    			'pendapatan' => $uangMasuk
    		 ];

    	return $log;
    }

    public function statistik($id_user='',$tanggal='')
    {
    	$log = [];
    	$userAll = [];

     	$tahun = $this->input->get('tahun');
     	if ($tahun == '') {
     		$this->tahun = date('Y');
     	}else{
     		$this->tahun = $tahun;
     	}

        $log = [
			'labels' => ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			'datasets' => $this->getStatistikToko(),
			'totalSeluruh' => 'Rp '.number_format($this->dapat,0,",",".")
		];

		echo json_encode($log);
    }

    public function getStatistikToko()
    {
    	$CI = &get_instance();
        $CI->load->model('UsersModel', 'um');
    	
    	$userAll = [];
    	$user = [];
        foreach ($CI->um->getUsers('','',['level' => '1'])->result() as $toko) {
        	$user = [
        		'label' => $toko->outlet_name,
        		'backgroundColor' => $toko->color,
        		'borderColor' => "#fff",
        		'data' => $this->pendapatanStatistikToko($toko->id)
        	];
        	array_push($userAll, $user);
        }

        return $userAll;
    }

    public function pendapatanStatistikToko($id='')
    {
    	$CI = &get_instance();
        $CI->load->model('UsersModel', 'um');
    
    	$user = [];
        foreach ($CI->um->getUsers('','',['level' => '1','id' => $id])->result() as $toko) {
			for($i=1; $i < 12; $i++)
			{
				if ($i != 12 || $i != 11 || $i != 10) {
					$b = '0'.$i;
				}else{
					$b = $i;
				}

			 $bulan = $this->pendapatan($toko->id,$this->tahun.'-'.$b)['pendapatan'];
			 $this->dapat += $this->pendapatan($toko->id,$this->tahun.'-'.$b)['pendapatan'];
			 array_push($user, $bulan);
			}
		}

       return $user;
    }


}

/* End of file DashboardModel.php */
/* Location: ./application/models/DashboardModel.php */