<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiServiceModel extends CI_Model {

    public $tabel = 'service_hp';
    public $id_user = 0;

	 public function __construct()
    {
        parent::__construct();
    }

    public function dtTransaksiService()
    {
          // Definisi
        $condition = '';

        $CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'tanggal_service','keterangan','biaya');
        // Set searchable column fields
        $CI->dt->column_search = array('tanggal_service','keterangan','biaya');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
            
         //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.tanggal_service',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.tanggal_service',date('Y-m'),'right'];
        }
            
        // INNER JOIN

        $condition = 
        [
          $user,
          $bulan
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $data[] = array(
                $i,
                $dt->tanggal_service,
                $dt->keterangan,
                'Rp '.number_format($dt->biaya,0,",","."),
                '<a href="javascript:;" onclick="getTransaksiServiceID('.$dt->id.')" data-toggle="modal" data-target="#edit" class="btn btn-info btn-xs item_edit"><i class="fa fa-edit"></i></a> ' .
                ' <a href="javascript:;" onclick="prosesDeTransService('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="'.
                $dt->id . '"><i class="fa  fa-trash"></i></a> '

            );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    public function getTransService($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE keterangan like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInTransService()
    {

        // Definisi
        $object = [
            'tanggal_service' => date('Y-m-d H:i:s'),
            'keterangan' => $this->input->post('keterangan'),
            'biaya' => $this->input->post('biaya'),
            'id_user' => $this->id_user,
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah service',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpTransService($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'tanggal_service' => date('Y-m-d H:i:s'),
            'keterangan' => $this->input->post('e_keterangan'),
            'biaya' => $this->input->post('e_biaya'),
            'id_user' => $this->id_user,
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDeTransService($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}