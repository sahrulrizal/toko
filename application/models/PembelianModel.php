<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PembelianModel extends CI_Model
{

    public $tabel = 'riwayat_transaksi_barang';
    public $id_user = 1;

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Model File');
    }

    public function dtPembelian()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'tanggal_transaksi', 'b.nama_barang','jumlah','b.harga_jual');
        // Set searchable column fields
        $CI->dt->column_search = array('tanggal_transaksi', 'b.nama_barang','jumlah','b.harga_jual' );
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*,b.nama_barang';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();

        //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.tanggal_transaksi',date('Y-m'),'right'];
        }


        $condition = 
        [
         ['join','barang b','b.id = '.$this->tabel.'.id_barang','inner'],
         ['where','id_jenis_transaksi',1],
         $user,
         $bulan,
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            // $total = ($dt->jumlah*$dt->harga_jual);
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->tanggal_transaksi,
                $dt->nama_barang,
                $dt->jumlah,
                'Rp '.number_format($dt->harga_satuan,0,",","."),
                'Rp '.number_format($dt->total_harga,0,",","."),
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                // '<a href="javascript:;" data-toggle="modal" data-target="#edit" onclick="getPembelianID('.$dt->id.')" class="btn btn-info btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa fa-edit"></i></a>' .
                // '<a href="javascript:;" onclick="prosesDePembelian('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPembelian($id='',$value='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE jumlah like "%'.$value.'%"',
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPembelian()
    {

        $id_bo = null;
       
        // DEFINISI UNTUK BARANG OUTLET
        $barang_outlet = $this->db->get_where('barang_outlet', ['id_barang' => $this->input->post('barang')]);
        
        if ($barang_outlet->num_rows() > 0 ) {
            $b_o = $barang_outlet->row();
            $id_bo = $b_o->id;
            $this->db->update('barang_outlet', ['stok' => $b_o->stok+$this->input->post('jumlah'), 'harga_satuan' => $this->input->post('harga_satuan')], ['id_barang' => $this->input->post('barang'),'id_user' => $this->id_user]);    
        }else{
            $ba = [
                'id_barang' => $this->input->post('barang'),
                'tanggal_beli' => $this->input->post('tanggal_transaksi'),
                'stok' => $this->input->post('jumlah'),
                'harga_satuan' => $this->input->post('harga_satuan'),
                'harga_jual' => $this->input->post('harga_satuan'),
                'id_user' => $this->id_user,
                'status' => 1
            ];

            $this->db->insert('barang_outlet', $ba);
            $id_bo = $this->db->insert_id();
        }

        // Definisi
        $object = [
            'id_jenis_transaksi' => 1,
            'id_barang' => $this->input->post('barang'),
            'tanggal_transaksi' => $this->input->post('tanggal_transaksi'),
            'harga_satuan' => $this->input->post('harga_satuan'),
            'total_harga' => $this->input->post('harga_total'),
            'jumlah' => $this->input->post('jumlah'),
            'id_user' => $this->id_user,
            'id_bo' => $id_bo
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah Pembelian',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    public function prosesUpPembelian($id='')
    {

        if ($id == '') {
            $id = $this->input->post('e_id');
        }

        $object = [
            'id_jenis_transaksi' => 3,
            'id_barang' => $this->input->post('barang'),
            'tanggal_transaksi' => $this->input->post('tanggal_transaksi'),
            'jumlah' => $this->input->post('jumlah'),
        ];

        $q = $this->db->update($this->tabel, $object, ['id' => $id]);
        $response = $this->db->get_where($this->tabel,$id);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil mengubah data',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

     public function prosesDePembelian($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}