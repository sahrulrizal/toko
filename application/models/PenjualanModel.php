<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PenjualanModel extends CI_Model
{

    public $tabel = 'riwayat_penjualan_barang';
    public $rtb = 'riwayat_transaksi_barang';
    public $bo = 'barang_outlet';
    public $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Model File');

    }

    public function dtPenjualan()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'created_date', 'kode_struk');
        // Set searchable column fields
        $CI->dt->column_search = array('created_date', 'kode_struk');
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();
            
        //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.created_date',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.created_date',date('Y-m'),'right'];
        }


         $condition = 
        [
         $user,
         $bulan
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $ks = "'".$dt->kode_struk."'";
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->created_date,
                $dt->kode_struk,
                // $dt->nama_barang,
                // $dt->jumlah,
                // 'Rp '.number_format($dt->harga_jual,0,",","."),
                // 'Rp '.number_format($dt->diskon,0,",","."),
                // 'Rp '.number_format($dt->total_all,0,",","."),
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->id . "</a>",
                '<a href="javascript:;" data-toggle="modal" data-target="#view" onclick="getPenjualanID('.$ks.')" class="btn btn-info btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa fa-eye"></i></a>',
                // '<a href="javascript:;" onclick="prosesDePenjualan('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                // $dt->id . '"><i class="fa  fa-trash"></i></a>',

            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function getPenjualan($id='',$value='',$kode_struk='')
    {
        $CI =& get_instance();
        $CI->load->model('SupportModel', 'sm');

        $getID = $this->input->get('id');
        if ($id == '') {
            $id = $getID;
        }

        $getValue = $this->input->get('value');
        if ($value == '') {
            $value = $getValue;
        }

        $getKodeStruk = $this->input->get('kode_struk');
        if ($kode_struk == '') {
            $kode_struk = $getKodeStruk;
        }

        if ($id != '') {
          
           $q = $CI->sm->tabel(array(
            'p' => 'q',
            'q' => "SELECT rpb.*,rtb.diskon FROM ".$this->tabel." rpb INNER JOIN ".$this->rtb." rtb ON rtb.id_pj_barang = rpb.id  WHERE id=".$id,
           )); 

        }else{
            
           if ($value != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel.' WHERE kode_struk like "%'.$value.'%"',
                ));
            }if ($kode_struk != '') {
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT rtb.*,b.nama_barang FROM ".$this->rtb." rtb INNER JOIN barang b ON b.id = rtb.id_barang WHERE kode_struk = '$kode_struk'",
                ));
            }else{
                $q = $CI->sm->tabel(array(
                    'p' => 'q',
                    'q' => "SELECT * FROM ".$this->tabel,
                ));
            } 

        }

        return $q;
    }

    public function prosesInPenjualan()
    {
        $obj = [];
        $object = [];
        $data = [];

        $CI =& get_instance();
        $CI->load->model('BarangOutletModel', 'bom');

        // Definisi
        $id     = $this->input->post('id');
        $struk  = $this->input->post('struk');
        $total_all  = $this->input->post('total_all');

        // CEK JIKA ADA KODE STRUK
       $row = $this->db->query("SELECT count(*) as jml FROM ".$this->tabel)->row();
       $struk = 'TR'.$row->jml;

       $tran = $this->db->insert($this->tabel, ['kode_struk' => $struk,'created_date' => date('Y-m-d H:i:s'),'id_user' => $this->id_user,'status'=> 1]);
       $id_pj = $this->db->insert_id();
       if ($tran) {

        for ($i=0; $i < count($id); $i++) { 

            // DEFINISIKAN SEBAGAI VARIABEL
            $list_barang    = $this->input->post('list-barang_'.$i);
            $list_jumlah    = $this->input->post('list-jumlah_'.$i);
            $list_diskon    = $this->input->post('list-diskon_'.$i);
            $harga_satuan   = $this->input->post('list-harga_satuan_'.$i);
            $total_harga    = $this->input->post('list-total_harga_'.$i);

            $this->bom->id_user = $this->id_user;
            $ob = (array) json_decode($this->bom->getBarang($list_barang));
            $this->db->update('barang_outlet',['stok' => intval($ob['response'][0]->stok) - $list_jumlah],['id_barang' => $list_barang, 'id_user' => $this->id_user]);

            $object = [
                'id_barang' => $list_barang,
                'id_pj_barang' => $id_pj,
                'jumlah' => $list_jumlah,
                'diskon' => $list_diskon,
                'id_jenis_transaksi' => 2,
                'tanggal_transaksi' => date('Y-m-d H:i:s'),
                'harga_satuan' => $harga_satuan,
                'kode_struk' => $struk,
                'total_harga' => $total_harga,
                'total_all' => $total_all,
                'id_user' => $this->id_user,
                'id_bo' => $ob['response'][0]->stok
            ];

            $this->db->insert($this->rtb, $object);
        }
       }

        $data = array(
            'request' => $object,
            'msg' => 'Berhasil tambah Penjualan',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => true
        );

        return json_encode($data);
    }

     public function prosesDePenjualan($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->update($this->tabel,['status'=> 0],['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}