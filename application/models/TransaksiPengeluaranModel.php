<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TransaksiPengeluaranModel extends CI_Model {

    public $tabel = 'riwayat_pengeluaran';
    public $id_user = 0;

	 public function __construct()
    {
        parent::__construct();
    }

    public function dtTransaksiPengeluaran()
    {
          // Definisi
        $condition = '';

        $CI =& get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'tgl_pengeluaran','kp.nama_pengeluaran','jumlah_pengeluaran');
        // Set searchable column fields
        $CI->dt->column_search = array('tgl_pengeluaran','kp.nama_pengeluaran','jumlah_pengeluaran');
         // Set select column fields
        $CI->dt->select = $this->tabel.'.*,nama_pengeluaran';
        // Set default order
        $CI->dt->order = array($this->tabel.'.id' => 'desc');

        $data = $row = array();
         
        //Jika ada id user 
        $id_user = $this->input->get('id_user');
        if($id_user != ''){
          $user = ['where',$this->tabel.'.id_user',$id_user];
        }else{
          $user = ['where',$this->tabel.'.id_user',$this->id_user];
        }

        //Jika ada Bulan
        $bulan = $this->input->get('bulan');
        if($bulan != ''){
          $bulan = ['like',$this->tabel.'.tgl_pengeluaran',$bulan,'right'];
        }else{
          $bulan = ['like',$this->tabel.'.tgl_pengeluaran',date('Y-m'),'right'];
        }
            
        // INNER JOIN

        $condition = 
        [
        	['join','kategori_pengeluaran kp','kp.id = '.$this->tabel.'.id_kategori_peng','inner'],
             $user,
            $bulan,
        ];

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST,$condition);
        
        $i = $_POST['start'];
        foreach($dataTabel as $dt){
            $i++;
            $data[] = array(
                $i,
                $dt->tgl_pengeluaran,
                $dt->nama_pengeluaran,
                'Rp '.number_format($dt->jumlah_pengeluaran,0,",","."),
                '<a href="javascript:;" onclick="prosesDeTransPeng('.$dt->id.')" class="btn btn-danger btn-xs item_edit" data="' .
                $dt->id . '"><i class="fa  fa-trash"></i></a> '

            );
        }
        
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST,$condition),
            "data" => $data,
        );
        
        // Output to JSON format
        return json_encode($output);
    }

    // public function getTransPeng($id='',$value='')
    // {
    //     $CI =& get_instance();
    //     $CI->load->model('SupportModel', 'sm');

    //     $getID = $this->input->get('id');
    //     if ($id == '') {
    //         $id = $getID;
    //     }

    //     $getValue = $this->input->get('value');
    //     if ($value == '') {
    //         $value = $getValue;
    //     }

    //     if ($id != '') {
          
    //        $q = $CI->sm->tabel(array(
    //         'p' => 'q',
    //         'q' => "SELECT * FROM ".$this->tabel." WHERE id=".$id,
    //        )); 

    //     }else{
            
    //        if ($value != '') {
    //             $q = $CI->sm->tabel(array(
    //                 'p' => 'q',
    //                 'q' => "SELECT * FROM ".$this->tabel.' WHERE TransPeng like "%'.$value.'%"',
    //             ));
    //         }else{
    //             $q = $CI->sm->tabel(array(
    //                 'p' => 'q',
    //                 'q' => "SELECT * FROM ".$this->tabel,
    //             ));
    //         } 

    //     }

    //     return $q;
    // }

    public function prosesInTransPeng()
    {

        // Definisi
        $idKategori = $this->input->post('kp');
        $biaya   = $this->input->post('biaya');

        # MENGAMBIL DARI TABEL KATEGORI PENGELUARAN#
        $kategori = $this->db->get_where('kategori_pengeluaran',['id' => $idKategori]);
        
        if ($kategori->num_rows() > 0) {
           $kategori = $kategori->row();
           $idKategori = $kategori->id;
        }else{
            $this->db->insert('kategori_pengeluaran', [
                'nama_pengeluaran' => $idKategori,
            ]);
            $idKategori = $this->db->insert_id();
        }

        $object = [
            'id_kategori_peng' => $idKategori,
            'tgl_pengeluaran' => date('Y-m-d H:i:s'),
            'jumlah_pengeluaran' => $this->input->post('biaya'),
            'id_user' => $this->id_user,
        ];

        $q = $this->db->insert($this->tabel, $object);
        $idInsert = $this->db->insert_id();
        $response = $this->db->get_where($this->tabel,$idInsert);
        $result = array(
            'first_row' => $this->db->get($this->tabel)->first_row(),
            'last_row' => $this->db->get($this->tabel)->last_row(),
            'previous_row' => $this->db->get($this->tabel)->previous_row(),
            'next_row' => $this->db->get($this->tabel)->next_row(),
        );

        $data = array(
            'request' => $object,
            'data' => $result,
            'msg' => 'Berhasil tambah pengeluaran',
            'dateTime' => date('Y-m-d H:i:s'),
            'success' => $response->num_rows() == 0 ? true : false
        );

        return json_encode($data);
    }

    // public function prosesUpTransPeng($id='')
    // {

    //     if ($id == '') {
    //         $id = $this->input->post('e_id');
    //     }

    //     $object = [
    //         'supplier' => $idSupplier,
    //         'kode' => $this->input->post('e_kode_TransPeng'),
    //         'kategori' => $idKategori,
    //         'nama_TransPeng' => $this->input->post('e_nama_TransPeng'),
    //         'harga_jual' => $this->input->post('e_harga_jual'),
    //         'harga_beli' => $this->input->post('e_harga_beli'),
    //         'stok' => $this->input->post('e_jumlah_stok'),
    //         'tanggal' => date('Y-m-d H:i:s'),
    //     ];

    //     $q = $this->db->update($this->tabel, $object, ['id' => $id]);
    //     $response = $this->db->get_where($this->tabel,$id);
    //     $result = array(
    //         'first_row' => $this->db->get($this->tabel)->first_row(),
    //         'last_row' => $this->db->get($this->tabel)->last_row(),
    //         'previous_row' => $this->db->get($this->tabel)->previous_row(),
    //         'next_row' => $this->db->get($this->tabel)->next_row(),
    //     );

    //     $data = array(
    //         'request' => $object,
    //         'data' => $result,
    //         'msg' => 'Berhasil mengubah data',
    //         'dateTime' => date('Y-m-d H:i:s'),
    //         'success' => $response->num_rows() == 0 ? true : false
    //     );

    //     return json_encode($data);
    // }

     public function prosesDeTransPeng($id=''){
        
        if ($id == '') {
            $id = $this->input->post('id');
        }

        $q = $this->db->delete($this->tabel,['id' => $id]);

        $arr = array(
            'msg' => "Berhasil hapus data",
        );

        return json_encode($arr);
     }
}