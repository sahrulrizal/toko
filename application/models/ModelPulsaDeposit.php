<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModelPulsaDeposit extends CI_Model
{

    public $tabel = 'pulsa_deposit';

    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Model File');
    }

    public function dtPulsaDeposit()
    {
        // Definisi
        $condition = '';

        $CI = &get_instance();
        $CI->load->model('DataTable', 'dt');

        // Set table name
        $CI->dt->table = $this->tabel;
        // Set orderable column fields
        $CI->dt->column_order = array(null, 'jenis', 'keterangan', 'harga');
        // Set searchable column fields
        $CI->dt->column_search = array('jenis', 'keterangan', 'harga');
        // Set select column fields
        $CI->dt->select = $this->tabel . '.*';
        // Set default order
        $CI->dt->order = array($this->tabel . '.id' => 'desc');

        $data = $row = array();

        // Fetch member's records
        $dataTabel = $this->dt->getRows($_POST, $condition);

        $i = $_POST['start'];
        foreach ($dataTabel as $dt) {
            $i++;
            $data[] = array(
                $i,
                // "<a href='" . site_url('back/barang/editBarang?id=' . $dt->id) . "' >" . $dt->nama_barang . "</a>",
                $dt->jenis,
                $dt->keterangan,
                $dt->harga,
            );
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->dt->countAll($condition),
            "recordsFiltered" => $this->dt->countFiltered($_POST, $condition),
            "data" => $data,
        );

        // Output to JSON format
        return json_encode($output);
    }

    public function intpulsadeposir($jenis, $keterangan, $harga)
    {
        $query = $this->db->query( "INSERT INTO pulsa_deposit(jenis,keterangan,harga) VALUES('$jenis','$keterangan','$harga')");
        return $query;
    }
}