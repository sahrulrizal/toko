<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransaksiBarang extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user  = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');

        $this->load->model('BarangModel', 'b');
        $this->b->id_user = $this->id_user;

        $this->load->model('ReturModel', 'r');
        $this->r->id_user = $this->id_user;

        $this->load->model('PembelianModel', 'p');
        $this->p->id_user = $this->id_user;

        $this->load->model('PenjualanModel', 'pj');
        $this->pj->id_user = $this->id_user;

        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
   
    }

    public function index()
    {
       $data = [
            'title' => 'TOKO :: TRANSAKSI BARANG',
            'menu'  => 'Barang',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'barang/barang', $data);
    }

    # ~PENJUALAN

    public function penjualan()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PENJUALAN',
            'menu'  => 'Penjualan',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiBarang/penjualan', $data);
    }

     // DATA TABLE
      public function dtPenjualan()
    {
       echo $this->pj->dtPenjualan();
    }
    
     // GET 
    public function getPenjualan()
    {
        echo $this->pj->getPenjualan();
    }

    // INSERT
     public function prosesInPenjualan()
    {
        echo $this->pj->prosesInPenjualan();
    }

    // UPDATE
     public function prosesUpPenjualan()
    {
        echo $this->pj->prosesUpPenjualan();
    }

    // DELETE
    public function prosesDePenjualan()
    {
        echo $this->pj->prosesDePenjualan();
    }

    # ~PEMBELIAN

    public function pembelian()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PEMBELIAN',
            'menu'  => 'Pembelian',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiBarang/pembelian', $data);
    }

    // DATA TABLE
      public function dtPembelian()
    {
       echo $this->p->dtPembelian();
    }
    
     // GET 
    public function getPembelian()
    {
        echo $this->p->getPembelian();
    }

    // INSERT
     public function prosesInPembelian()
    {
        echo $this->p->prosesInPembelian();
    }

    // UPDATE
     public function prosesUpPembelian()
    {
        echo $this->p->prosesUpPembelian();
    }

    // DELETE
    public function prosesDePembelian()
    {
        echo $this->p->prosesDePembelian();
    }

    
    # ~RETUR
    
    public function retur()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI RETUR',
            'menu'  => 'Retur',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiBarang/retur', $data);
    }

    // DATA TABLE
      public function dtRetur()
    {
       echo $this->r->dtRetur();
    }
    
     // GET 
    public function getRetur()
    {
        echo $this->r->getRetur();
    }

    // INSERT
     public function prosesInRetur()
    {
        echo $this->r->prosesInRetur();
    }

    // UPDATE
     public function prosesUpRetur()
    {
        echo $this->r->prosesUpRetur();
    }

    // DELETE
    public function prosesDeRetur()
    {
        echo $this->r->prosesDeRetur();
    }

    # ~DISKON

    public function diskon()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI DISKON',
            'menu'  => 'Diskon',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiBarang/diskon', $data);
    }

    public function getBarang()
    {
        $this->b->getBarang();
    }

    public function dtBarang()
    {
       echo $this->b->dtBarang();
    }



}