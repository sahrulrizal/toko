<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransaksiPengeluaran extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');
        $this->load->model('TransaksiPengeluaranModel', 'b');
        $this->b->id_user = $this->id_user;

        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
    }

    public function index()
    {
       $data = [
            'title' => 'TOKO :: TRANSAKSI PENGELUARAN',
            'menu'  => 'Transaksi Pengeluaran',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiPengeluaran/transaksiPengeluaran', $data);
    }

    // DATA TABLE
      public function dtTransaksiPengeluaran()
    {
       echo $this->b->dtTransaksiPengeluaran();
    }
    
    // INSERT
     public function prosesInTransPeng()
    {
        echo $this->b->prosesInTransPeng();
    }

    // // UPDATE
    //  public function prosesUpTransPeng()
    // {
    //     echo $this->b->prosesUpTransPeng();
    // }

    // DELETE
    public function prosesDeTransPeng()
    {
        echo $this->b->prosesDeTransPeng();
    }


}