<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BarangOutlet extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');
        
        $this->load->model('BarangOutletModel', 'b');
        $this->b->id_user = $this->id_user;

        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
    }

    public function index()
    {
       $data = [
            'title' => 'TOKO :: BARANG',
            'menu'  => 'Barang',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'barang/barang_outlet', $data);
    }

    // DATA TABLE
    public function dtBarang()
    {
       echo $this->b->dtBarang();
    }

    // GET ID / GET ALL TABLE
    public function getBarang()
    {
       echo $this->b->getBarang();
    }
 
    public function getBarangSelect()
    {
       
        $cari = $this->input->get('search');
        $ok = (array)json_decode($this->b->getBarang('',$cari));
        
        $list = [];

        for ($i=0; $i < $ok['count']; $i++) { 
             $list[$i]['id'] = $ok['response'][$i]->id;
             $list[$i]['text'] = $ok['response'][$i]->nama_barang;  
        }
        echo json_encode($list);
    }

    // INSERT
     public function prosesInBarang()
    {
        echo $this->b->prosesInBarang();
    }

    // UPDATE
     public function prosesUpBarang()
    {
        echo $this->b->prosesUpBarang();
    }

    // DELETE
    public function prosesDeBarang()
    {
        echo $this->b->prosesDeBarang();
    }

}