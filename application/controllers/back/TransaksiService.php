<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransaksiService extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    public $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');

        $this->load->model('TransaksiServiceModel', 'b');
        $this->b->id_user = $this->id_user;

        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
    }

    public function index()
    {
       $data = [
            'title' => 'TOKO :: TRANSAKSI SERVICE',
            'menu'  => 'Transaksi Service',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u,
        ];

        $this->load->view($this->pathRoot.'transaksiService/transaksiService', $data);
    }

    // DATA TABLE
      public function dtTransaksiService()
    {
       echo $this->b->dtTransaksiService();
    }

     // GET 
    public function getTransService()
    {
        echo $this->b->getTransService();
    }
    
    // INSERT
     public function prosesInTransService()
    {
        echo $this->b->prosesInTransService()   ;
    }

    // UPDATE
     public function prosesUpTransService()
    {
        echo $this->b->prosesUpTransService();
    }

    // DELETE
    public function prosesDeTransService()
    {
        echo $this->b->prosesDeTransService();
    }

}