<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	
    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user  = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');

        $this->load->model('BarangModel', 'b');
        $this->b->id_user = $this->id_user;

        $this->load->model('ReturModel', 'r');
        $this->r->id_user = $this->id_user;

        $this->load->model('PembelianModel', 'p');
        $this->p->id_user = $this->id_user;

        $this->load->model('PenjualanModel', 'pj');
        $this->pj->id_user = $this->id_user;

        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
   
    }

    public function index()
    {
       $data = [
            'title' => 'TOKO :: TRANSAKSI BARANG',
            'menu'  => 'Barang',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        echo "ngapain ?";
    }

    # ~PENJUALAN

    public function penjualan()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PENJUALAN',
            'menu'  => 'Penjualan',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/penjualan', $data);
    }

    # ~PEMBELIAN

    public function pembelian()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PEMBELIAN',
            'menu'  => 'Pembelian',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/pembelian', $data);
    }

    # ~PENGELUARAN

    public function pengeluaran()
    {
         $data = [
            'title' => 'TOKO :: TRANSAKSI PENGELUARAN',
            'menu'  => 'Transaksi Pengeluaran',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/pengeluaran', $data);
    }

    
    # ~RETUR
    
    public function retur()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI RETUR',
            'menu'  => 'Retur',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/retur', $data);
    }

    # ~PulsaDanPaket

    public function pulsaDanLagu()
    {
         $data = [
            'title' => 'TOKO :: PULSA TRANSFER DAN LAGU',
            'menu'  => 'Pulsa Transfer Dan Lagu',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/pulsaDanLagu', $data);
    }


    # ~DISKON

    public function diskon()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI DISKON',
            'menu'  => 'Diskon',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/diskon', $data);
    }

    # ~PULSA DEPOSIT

    public function pulsaDeposit()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PULSA DEPOSIT',
            'menu'  => 'PULSA DEPOSIT',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/pulsaDeposit', $data);
    }


    // ~PAKET ISI ULANG

    public function paketIsiUlang()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PAKET ISI ULANG',
            'menu'  => 'PAKET ISI ULANG',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'laporan/paketIsiUlang', $data);
    }

    // ~ SERVICE
     public function service()
    {
       $data = [
            'title' => 'TOKO :: TRANSAKSI SERVICE',
            'menu'  => 'TRANSAKSI SERVICE',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u,
        ];

        $this->load->view($this->pathRoot.'laporan/service', $data);
    }

    // GET ID / GET ALL TABLE
    public function getToko()
    {
        $obj = ['level' => 1];
       echo json_encode($this->u->getUsers('','',$obj)->result());
    }

}

/* End of file Laporan.php */
/* Location: ./application/controllers/back/Laporan.php */