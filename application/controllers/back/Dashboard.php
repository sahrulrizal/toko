<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	private $pathRoot = 'back/';
	private $pathIncl = 'incl/';
	private $id_user = 0;

	public function __construct()
	{
		parent::__construct();
		header('Access-Control-Allow-Origin:*');
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

		$this->id_user = $this->session->userdata('id');

		$this->load->model('UsersModel', 'u');
		$this->load->model('DashboardModel', 'dm');
		$this->u->id_user = $this->id_user;
	}

	public function index()
	{
		
		$data = [
			'title' => 'TOKO :: DASHBOARD'.$this->id_user,
			'menu'  => 'Dashboard',
			'pathFolder'=> $this->pathIncl,
			'user' => $this->u,
			'dm' => $this->dm
		];

		$this->load->view($this->pathRoot.'/dashboard/dashboard', $data);

	}

	public function statistik()
	{
		echo $this->dm->statistik();
	}

	public function pendapatan()
	{
		$id = $this->input->get('id');
		$bulan = $this->input->get('bulan');

		echo json_encode($this->dm->pendapatan($id,$bulan));
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/back/Dashboard.php */