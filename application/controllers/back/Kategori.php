<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kategori extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('KategoriModel', 'k');
    }

    public function index()
    {
      echo "...";
    }

    public function getKategoriBarang()
    {
        echo $this->k->getKategoriBarang();
    }

    public function getKategoriPengeluaran()
    {
        echo $this->k->getKategoriPengeluaran();
    }
}