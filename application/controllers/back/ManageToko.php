<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageToko extends CI_Controller {

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
	private $id_user = 0;
	
    public function __construct()
	{
		parent::__construct();
		 $this->id_user = $this->session->userdata('id');

		$this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
	}

	public function index()
    {
       $data = [
            'title' => 'TOKO :: MANAGE TOKO',
            'menu'  => 'Manage Toko',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'manageToko/manageToko', $data);
    }

}

/* End of file ManageToko.php */
/* Location: ./application/controllers/back/ManageToko.php */