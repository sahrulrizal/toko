<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('UsersModel', 'u');
        $this->id_user = $this->session->userdata('id');
        $this->u->id_user = $this->id_user;
    }

    public function index()
    {
       $data = [
            'title' => 'TOKO :: PROFILE',
            'menu'  => 'Profile',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'profile/profile', $data);
    }

}