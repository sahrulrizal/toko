<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pulsa_paket extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user  = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');
       
        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;

        $this->load->model('ModelPulsaTransferLagu', 'm');
        $this->m->id_user = $this->id_user;

        $this->load->model('DepositModel', 'd');
        $this->d->id_user = $this->id_user;

        $this->load->model('PIUModel', 'piu');
        $this->piu->id_user = $this->id_user;

    }


    public function pulsaTransferDanLagu()
    {
        $data = [
            'title' => 'TOKO :: PULSA TRANSFER DAN LAGU',
            'menu' => 'Pulsa Transfer dan Lagu',
            'pathFolder' => $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot . 'pulsa_paket/pulsaTransferDanLagu', $data);
    }

    // DATA TABLE
      public function dtPulsaTransferLagu()
    {
       echo $this->m->dtPulsaTransferLagu();
    }
    
     // GET 
    public function getPTL()
    {
        echo $this->m->getPTL();
    }

    // INSERT
     public function prosesInPTL()
    {
        echo $this->m->prosesInPTL();
    }
// id
    public function getpulsa()
    {
        echo $this->m->getPulsa;
    }

    // UPDATE
     public function prosesUpPTL()
    {
        echo $this->m->prosesUpPTL();
    }

    // DELETE
    public function prosesDePTL()
    {
        echo $this->m->prosesDePTL();
    }


    // ~DEPOSIT
    public function deposit()
    {
        $data = [
            'title' => 'TOKO :: DEPOSIT',
            'menu' => 'Deposit',
            'pathFolder' => $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot . 'pulsa_paket/deposit', $data);
    }

    // DATA TABLE
      public function dtDeposit()
    {
       echo $this->d->dtDeposit();
    }

     // GET 
    public function getDeposit()
    {
        echo $this->d->getDeposit();
    }

    // INSERT
     public function prosesInDeposit()
    {
        echo $this->d->prosesInDeposit();
    }

    // UPDATE
     public function prosesUpDeposit()
    {
        echo $this->d->prosesUpDeposit();
    }

    // DELETE
    public function prosesDeDeposit()
    {
        echo $this->d->prosesDeDeposit();
    }

    // ~DEPOSIT BULK
    public function depositBulk()
    {
        $data = [
            'title' => 'TOKO :: DEPOSIT BULK',
            'menu' => 'Deposit Bulk',
            'pathFolder' => $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot . 'pulsa_paket/depositBulk', $data);
    }

    // DATA TABLE
      public function dtDepositBulk()
    {
       echo $this->d->dtDepositBulk();
    }

     // GET 
    public function getDepositBulk()
    {
        echo $this->d->getDepositBulk();
    }

    // INSERT
     public function prosesInDepositBulk()
    {
        echo $this->d->prosesInDepositBulk();
    }

    // UPDATE
     public function prosesUpDepositBulk()
    {
        echo $this->d->prosesUpDepositBulk();
    }

    // DELETE
    public function prosesDeDepositBulk()
    {
        echo $this->d->prosesDeDepositBulk();
    }

    // // ~PAKET ISI ULANG

    // public function paketIsiUlang()
    // {
    //     $data = [
    //         'title' => 'TOKO :: PAKET ISI ULANG',
    //         'menu' => 'Paket Isi Ulang',
    //         'pathFolder' => $this->pathIncl,
    //     ];

    //     $this->load->view($this->pathRoot . 'pulsa_paket/paketIsiUlang', $data);
    // }

    # ~DEPOSIT 2

    public function paketIsiUlangDeposit1()
    {
        $data = [
            'title' => 'TOKO :: PAKET ISI ULANG DEPOSIT 1',
            'menu' => 'Paket Isi Ulang Deposit 1',
            'pathFolder' => $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot . 'pulsa_paket/paketIsiUlangDeposit1', $data);
    }

    // DATA TABLE
      public function dtPIUD1()
    {
       echo $this->piu->dtPIUD1();
    }

     // GET 
    public function getPIUD1()
    {
        echo $this->piu->getPIUD1();
    }

    // INSERT
     public function prosesInPIUD1()
    {
        echo $this->piu->prosesInPIUD1();
    }

    // UPDATE
     public function prosesUpPIUD1()
    {
        echo $this->piu->prosesUpPIUD1();
    }

    // DELETE
    public function prosesDePIUD1()
    {
        echo $this->piu->prosesDePIUD1();
    }

    # ~DEPOSIT 2

    public function paketIsiUlangDeposit2()
    {
        $data = [
            'title' => 'TOKO :: PAKET ISI ULANG DEPOSIT 2',
            'menu' => 'Paket Isi Ulang Deposit 2',
            'pathFolder' => $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot . 'pulsa_paket/paketIsiUlangDeposit2', $data);
    }

    // DATA TABLE
      public function dtPIUD2()
    {
       echo $this->piu->dtPIUD2();
    }

     // GET 
    public function getPIUD2()
    {
        echo $this->piu->getPIUD2();
    }

    // INSERT
     public function prosesInPIUD2()
    {
        echo $this->piu->prosesInPIUD2();
    }

    // UPDATE
     public function prosesUpPIUD2()
    {
        echo $this->piu->prosesUpPIUD2();
    }

    // DELETE
    public function prosesDePIUD2()
    {
        echo $this->piu->prosesDePIUD2();
    }

    # ~MATAULI

    public function paketIsiUlangMatauli()
    {
        $data = [
            'title' => 'TOKO :: PAKET ISI ULANG MATAULI',
            'menu' => 'Paket Isi Ulang Matauli',
            'pathFolder' => $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot . 'pulsa_paket/paketIsiUlangMatauli', $data);
    }

    // DATA TABLE
      public function dtPIUMatauli()
    {
       echo $this->piu->dtPIUMatauli();
    }

     // GET 
    public function getPIUMatauli()
    {
        echo $this->piu->getPIUMatauli();
    }

    // INSERT
     public function prosesInPIUMatauli()
    {
        echo $this->piu->prosesInPIUMatauli();
    }

    // UPDATE
     public function prosesUpPIUMatauli()
    {
        echo $this->piu->prosesUpPIUMatauli();
    }

    // DELETE
    public function prosesDePIUMatauli()
    {
        echo $this->piu->prosesDePIUMatauli();
    }

}