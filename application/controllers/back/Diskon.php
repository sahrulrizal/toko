<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diskon extends CI_Controller {

	 private $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');
        
        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;

        $this->load->model('DiskonModel', 'dm');
        $this->dm->id_user = $this->id_user;
    }

   # ~DISKON

    public function dtDiskon()
    {
    	echo $this->dm->dtDiskon();
    }

}

/* End of file Diskon.php */
/* Location: ./application/controllers/back/Diskon.php */