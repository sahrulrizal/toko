<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransaksiPulsa_paket extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
        $this->id_user = $this->session->userdata('id');

        $this->load->model('Pulsa_paketModel', 'p');
        $this->p->id_user = $this->id_user;

        $this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;

    }

    # ~pulsaTransferDanLagu

    public function pulsaTransferDanLagu()
    {
        $data = [
            'title' => 'TOKO :: PULSA TRANSFER DAN LAGU',
            'menu'  => 'Pulsa Transfer Dan Lagu',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiPulsa_paket/pulsaTransferDanLagu', $data);
    }

     // DATA TABLE
      public function dtPTDL()
    {
       echo $this->p->dtPTDL();
    }
    
     // GET 
    public function getPTDL()
    {
        echo $this->p->getPTDL();
    }

    // INSERT
     public function prosesInPTDL()
    {
        echo $this->p->prosesInPTDL();
    }

    // UPDATE
     public function prosesUpPTDL()
    {
        echo $this->p->prosesUpPTDL();
    }

    // DELETE
    public function prosesDePTDL()
    {
        echo $this->p->prosesDePTDL();
    }

    // ~PULSA DEPOSIT

    public function pulsaDeposit()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PULSA DEPOSIT',
            'menu'  => 'Transaksi Pulsa Deposit',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiPulsa_paket/pulsaDeposit', $data);
    }

     // DATA TABLE
      public function dtPD()
    {
       echo $this->p->dtPD();
    }
    
     // GET 
    public function getPD()
    {
        echo $this->p->getPD();
    }

    // INSERT
     public function prosesInPD()
    {
        echo $this->p->prosesInPD();
    }

    // UPDATE
     public function prosesUpPD()
    {
        echo $this->p->prosesUpPD();
    }

    // DELETE
    public function prosesDePD()
    {
        echo $this->p->prosesDePD();
    }

    // ~PAKET ISI ULANG

    public function paketIsiUlang()
    {
        $data = [
            'title' => 'TOKO :: TRANSAKSI PAKET ISI ULANG',
            'menu'  => 'Paket Isi Ulang',
            'pathFolder'=> $this->pathIncl,
            'user' => $this->u
        ];

        $this->load->view($this->pathRoot.'transaksiPulsa_paket/paketIsiUlang', $data);
    }

    // DATA TABLE
      public function dtPIU()
    {
       echo $this->p->dtPIU();
    }
    
     // GET 
    public function getPIU()
    {
        echo $this->p->getPIU();
    }

    // INSERT
     public function prosesInPIU()
    {
        echo $this->p->prosesInPIU();
    }

    // UPDATE
     public function prosesUpPIU()
    {
        echo $this->p->prosesUpPIU();
    }

    // DELETE
    public function prosesDePIU()
    {
        echo $this->p->prosesDePIU();
    }

}