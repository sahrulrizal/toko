<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public $id_user = 0;
	public function __construct()
	{
		parent::__construct();
		 $this->id_user = $this->session->userdata('id');

		$this->load->model('UsersModel', 'u');
        $this->u->id_user = $this->id_user;
	}

	public function upSelfUsers()
	{
		$u = $this->u->getUsers($this->id_user)->row();

		if ($this->input->post('password') == '') {
			$password = $u->password;
			$password_open = $u->password_open;
		}else{
			$password = md5($this->input->post('password'));
			$password_open = $this->input->post('password');
		}

		$obj = [
			'outlet_name' => $this->input->post('outlet_name'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'no_telp' => $this->input->post('no_telp'),
			'username' => $this->input->post('username'),
			'password' => $password,
			'password_open' => $password_open,
			'alamat' => $this->input->post('alamat')
		];
		echo json_encode($this->u->upUsers($obj,$this->id_user)); 
	}

	public function upUsers()
	{
		$obj = [
			'outlet_name' => $this->input->post('outlet_name'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'no_telp' => $this->input->post('no_telp'),
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'password_open' => $this->input->post('password'),
			'alamat' => $this->input->post('alamat')
		];

		$id = $this->input->post('id');
		
		echo json_encode($this->u->upUsers($obj,$id));
	}


    // DATA TABLE
    public function dtUsers()
    {
       echo $this->u->dtUsers();
    }

    // GET ID / GET ALL TABLE
    public function getUsers()
    {
       echo $this->u->getUsers();
    }

     public function getUsersID()
    {
        $id = $this->input->get('id');
        echo json_encode($this->u->getUsers($id)->row());
    }

}

/* End of file User.php */
/* Location: ./application/controllers/back/User.php */