<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Controller
{

    private $pathRoot = 'back/';
    private $pathIncl = 'incl/';
    private $id_user  = 0;

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->id_user = $this->session->userdata('id');
        
        $this->load->model('SupplierModel', 's');
        $this->s->id_user = $this->id_user;
    }

    public function index()
    {
      echo "...";
    }

    public function getSupplier()
    {
        echo $this->s->getSupplier();
    }

    public function dtSupplier()
    {
        echo $this->s->dtSupplier();
    }

}