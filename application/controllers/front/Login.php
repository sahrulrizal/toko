<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    private $pathRoot = 'front/';
    private $pathIncl = 'incl/';

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('LoginModel', 'lm');
        
        // if ($this->uri->segment(2) != "logout") {
        //     if ($this->session->userdata('id')) {
        //         if ($this->session->userdata('level') == 1) {
        //             redirect('admin');
        //         }elseif ($this->session->userdata('level') == 2) {
        //             redirect('dosen');
        //         }elseif ($this->session->userdata('level') == 3) {
        //             redirect('mahasiswa');
        //         }
        //     }
        // }

        // $this->tgl = $this->input->get('tanggal') ? $this->input->get('tanggal') : date('Y-m-d');
    }

    public function index()
    {
        $data = [
            'title' => 'Login',
            'pathFolder'=> $this->pathIncl,
        ];

        $this->load->view($this->pathRoot.'login/login', $data);
    }

    public function prosesKeLogin()
    {
        $this->lm->username = $this->input->post('username');
        $this->lm->password = $this->input->post('password');
        $this->lm->prosesKeLogin();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        $this->session->unset_userdata(['id','level']);
        redirect('/');
    }

   

}