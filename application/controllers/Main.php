<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

    }

    public function index()
    {
        $data['title'] = '';
        $data['namaMenu'] = '';
        $data['aktif'] = '';


        $this->load->view('main/index', $data, false);

        // $this->load->helper('file');
        // $pathController = APPPATH.'controllers/';
        // $pathModels = APPPATH.'models/';
        // $pathViews = APPPATH.'views/';
        // // $data = ;
        // // if (!write_file($pathController.'File.php', $data))
        // // {
        // //     echo 'Unable to write the file';
        // // }
        // // else
        // // {
        // //     echo 'File written!';
        // // }
        // if (copy($pathController."Users.php",$pathController."Cluster.php")){
        //     $this->gantiKata($pathController.'Cluster.php',"Users","Cluster");
        // }

        // if (copy($pathModels."UsersModel.php",$pathModels."ClusterModel.php")){
        //     $this->gantiKata($pathModels."ClusterModel.php","UsersModel","ClusterModel");
        //     $this->gantiKata($pathModels."ClusterModel.php","Users","Cluster");
        // }

        // if (copy($pathViews."users/users.php",$pathViews."cluster/cluster.php")){

        // }

        // $data = file($pathController."Cluster.php"); // reads an array of lines
        // replace_a_line($data);
        // $data = array_map('replace_a_line',$data);
        //  file_put_contents($pathController."File.php", implode('', $data));

    }

    private function gantiKata($path,$ganti,$jadi) {
        $oldMessage = $ganti;

        $deletedFormat = $jadi;

        //read the entire string
        $str=file_get_contents($path);

        //replace something in the file string - this is a VERY simple example
        $str=str_replace("$oldMessage", "$deletedFormat",$str);

        //write the entire string
        file_put_contents($path, $str);
     }
   
}