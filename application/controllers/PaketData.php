<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PaketData extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        header('Access-Control-Allow-Origin:*');
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

        $this->load->model('PaketDataModel', 'tm');
        $this->tgl = $this->input->get('tanggal') ? $this->input->get('tanggal') : date('Y-m-d');
    }

    public function index()
    {
        $data['title'] = '';
        $data['namaMenu'] = '';
        $data['aktif'] = '';

        $data['tgl'] = $this->tgl;

        #Sukses
        $data['totalSukses'] = json_decode($this->tm->totalPaketData('1',$this->tgl));
        
        #Gagal
        $data['totalGagal'] = json_decode($this->tm->totalPaketData('0',$this->tgl));

        #Income Sukses
        $data['incomeSukses'] = json_decode($this->tm->totalIncomePaketData('1',$this->tgl));

        #Income Sukses
        $data['incomeGagal'] = json_decode($this->tm->totalIncomePaketData('0',$this->tgl));

        // $incomeSukses->response[0]->total_income;
        // $incomeGagal->response[0]->total_income == null ? 0 : 0;
        
        $this->load->view('paketdata/paketdata', $data, false);
    }

    public function getPaketData()
    {
        echo $this->tm->getPaketData($this->tgl);
    }

    public function getPaketDataID()
    {
        $id = $this->input->get('id');
        echo $this->tm->getPaketDataID($id);
    }

}