 <!-- head -->
 <?php $this->load->view($pathFolder . 'head'); ?>

 <body class="fixed-navbar">
     <div class="page-wrapper">
         <!-- START HEADER-->
         <?php $this->load->view($pathFolder . 'header'); ?>
         <!-- END HEADER-->
         <!-- START SIDEBAR-->
         <?php $this->load->view($pathFolder . 'sidebar'); ?>
         <!-- END SIDEBAR-->
         <div class="content-wrapper">
             <!-- START PAGE CONTENT-->
             <?php $this->load->view($pathFolder . 'breadcrumb'); ?>
             <div class="page-content fade-in-up">

                 <div class="ibox">
                     <div class="ibox-head">
                         <div class="ibox-title">Tambah Data Pulsa Transfer & Lagu <a onclick="alert('hello gaes')" class="btn btn-info btn-xs item_edit"></a></div>
                         <div class="ibox-tools">
                             <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                             <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                         </div>
                     </div>
                     <div class="ibox-body">
                         <form action="javascript:;" id="inForm" method="POST">
                             <div class="row">
                                 <div class="col-md-6 form-group">
                                     <label>Jenis Pulsa Transfer & Lagu</label><br>
                                     <input required class="form-control" type="text" name="keterangan" id="keterangan" placeholder="Keterangan pulsa transfer dan lagu">
                                 </div>
                                 <div class="col-sm-6 form-group">
                                     <label>Harga</label>
                                     <div class="input-group">
                                         <div class="input-group-addon">Rp.</div>
                                         <input class="form-control" required type="number" name="harga" id="harga"
                                             placeholder="Harga jual">
                                     </div>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <button class="btn btn-primary" type="submit" id="btn_simpan">Simpan</button>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>

             <div class="ibox">
                 <div class="ibox-head">
                     <div class="ibox-title">Data Pulsa Transfer & Lagu</div>
                 </div>
                 <div class="ibox-body">
                     <table class="table table-striped" id="contoh">
                         <thead>
                             <tr>
                                 <th>No</th>
                                 <th>Jenis Pulsa Transfer dan Lagu</th>
                                 <th>Harga</th>
                                 <th>Action</th>

                             </tr>
                         </thead>
                         <tbody style="font-size: 12px;">
                             <tr>
                                 <td>1</td>
                                 <td>P Indosat 20K</td>
                                 <td>Rp. 22000</td>
                             </tr>
                         </tbody>
                     </table>

                 </div>
             </div>
             <!-- END PAGE CONTENT-->
             <?php $this->load->view($pathFolder . 'footer'); ?>
         </div>
     </div>

     <!-- Modal -->
    <div id="edit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
        </div>
        <form id="upForm" action="javascript:void(0)" method="POST">
        <div class="modal-body">
                <div class="row">
                      <div class="col-md-8 form-group">
                        <label>Jenis Pulsa Transfer & Lagu</label><br>
                        <input class="form-control" type="hidden" name="e_id">
                        <input class="form-control" type="text" name="e_keterangan" placeholder="Isi keterangan service">
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Biaya</label>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input class="form-control" type="number" name="e_harga" placeholder="Biaya service">
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form> 
        </div>
    </div>

     <!-- BEGIN THEME CONFIG PANEL-->

     <!-- END THEME CONFIG PANEL-->


     <!-- CORE PLUGINS-->
     <?php $this->load->view($pathFolder . 'script'); ?>
     <script type="text/javascript">
        $(document).ready(function() {
            showTable();
            prosesInPTL();
            prosesUpPTL();
        });     

    function hapusAtributUp() {
        $('input[name=e_keterangan]').val('');
        $('input[name=e_harga]').val('');
    }

    function hapusAtributIn() {
        $('input[name=keterangan]').val('');
        $('input[name=harga]').val('');
    }

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/pulsa_paket/dtPulsaTransferLagu");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
            "columnDefs": [{ 
                "targets": [0],
                "orderable": false
            }]
        });
     }

    function getPTLID(id) {
        
        hapusAtributUp();

        $.ajax({
            url: '<?=base_url("back/pulsa_paket/getPTL?id=");?>'+id,
            type: 'GET',
            dataType : 'json',
        })
        .done(function(data) {

            var val = data.response[0];

            $('input[name=e_id]').val(val.id);
            $('input[name=e_keterangan]').val(val.keterangan);
            $('input[name=e_harga]').val(val.harga);

          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
    }


    function prosesInPTL() {
        $('#inForm').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/pulsa_paket/prosesInPTL");?>',
            type: 'POST',
            dataType : 'JSON',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            hapusAtributIn();
             Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesUpPTL() {
        $('#upForm').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/pulsa_paket/prosesUpPTL");?>',
            type: 'POST',
            dataType : 'json',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesDePTL(ID) {
       Swal.fire({
          title: 'Apakah anda yakin ingin menghapus data ini ?',
          text: "Anda tidak akan dapat mengembalikan data ini kembali!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak',
        }).then((result) => {
          if (result.value) {
            $.ajax({
                url: '<?=base_url("back/pulsa_paket/prosesDePTL");?>',
                type: 'POST',
                dataType: 'JSON',
                data : {id : ID}
            })
            .done(function(data) {
                console.log(data.msg);
                showTable();
                Swal.fire(
                    'Terhapus!',
                  data.msg,
                  'success'
                  );
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
          }
        })
    }

     </script>
 </body>

 </html> 