 <!-- head -->
 <?php $this->load->view($pathFolder . 'head');?>

 <body class="fixed-navbar">
     <div class="page-wrapper">
         <!-- START HEADER-->
         <?php $this->load->view($pathFolder . 'header');?>
         <!-- END HEADER-->
         <!-- START SIDEBAR-->
         <?php $this->load->view($pathFolder . 'sidebar');?>
         <!-- END SIDEBAR-->
         <div class="content-wrapper">
             <!-- START PAGE CONTENT-->
             <?php $this->load->view($pathFolder . 'breadcrumb');?>
             <div class="page-content fade-in-up">

                 <div class="ibox">
                     <div class="ibox-head">
                         <div class="ibox-title">Tambah Data Paket Isi Ulang</div>
                         <div class="ibox-tools">
                             <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                             <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                         </div>
                     </div>
                     <div class="ibox-body">
                         <form>
                             <div class="row">
                                 <div class="col-sm-4 form-group">
                                     <label>Jenis Paket Isi Ulang</label>
                                     <select class="form-control">
                                         <option value="">Paket Isi Ulang Deposit 1</option>
                                         <option value="">Paket Isi Ulang Deposit 2</option>
                                         <option value="">Paket Isi Ulang Matauli</option>
                                     </select>
                                 </div>
                                 <div class="col-md-4 form-group">
                                     <label>Nama Paket Isi Ulang</label><br>
                                     <input class="form-control" type="text" name="jb"
                                         placeholder="Keterangan paket isi ulang">
                                 </div>
                                 <div class="col-sm-4 form-group">
                                     <label>Harga</label>
                                     <div class="input-group">
                                         <div class="input-group-addon">Rp.</div>
                                         <input class="form-control" type="number" placeholder="Harga jual">
                                     </div>
                                 </div>
                             </div>
                             <div class="form-group">
                                 <button class="btn btn-primary" type="submit">Simpan</button>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>

             <div class="ibox">
                 <div class="ibox-head">
                     <div class="ibox-title">Data Paket Isi Ulang</div>
                 </div>
                 <div class="ibox-body">
                     <table class="table table-striped" id="contoh">
                         <thead>
                             <tr>
                                 <th>No</th>
                                 <th>Jenis Paket Isi Ulang</th>
                                 <th>Keterangan Paket Isi Ulang</th>
                                 <th>Harga</th>
                             </tr>
                         </thead>
                         <tbody style="font-size: 12px;">
                             <tr>
                                 <td>1</td>
                                 <td>Paket Isi Ulang Matauli</td>
                                 <td>Tsel 4</td>
                                 <td>Rp. 43000</td>
                             </tr>
                         </tbody>
                     </table>
                 </div>
             </div>
             <!-- END PAGE CONTENT-->
             <?php $this->load->view($pathFolder . 'footer');?>
         </div>
     </div>

     <!-- BEGIN THEME CONFIG PANEL-->

     <!-- END THEME CONFIG PANEL-->


     <!-- CORE PLUGINS-->
     <?php $this->load->view($pathFolder . 'script');?>
     <script type="text/javascript">
     $('#contoh').DataTable({
         // Processing indicator
         "processing": true,
         // DataTables server-side processing mode
         "serverSide": true,
         "scrollX": true,
         // Initial no order.
         "order": [],
         // Load data from an Ajax source
         "ajax": {
             "url": "<?=base_url("back/pulsa_paket/getpaketisiulang");?>",
             "type": "POST"
         },
         //Set column definition initialisation properties
         "columnDefs": [{
             "targets": [0],
             "orderable": false
         }]
     });
     </script>
 </body>

 </html>