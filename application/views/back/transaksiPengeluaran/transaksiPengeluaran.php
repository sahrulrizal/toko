 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">

                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Tambah Transaksi Pengeluaran</div>
                        <div class="ibox-tools">
                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <form id='formIn' method="post" action="javascript:void(0)">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label>Kategori Pengeluaran</label><br>
                                    <select required id="KP" style="width: 100%;" class="form-control" name="kp">
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Biaya</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp.</div>
                                        <input class="form-control" type="number" name="biaya" placeholder="Biaya pengeluaran">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Riwayat Pengeluaran</div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped" id="contoh">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Kategori Pengeluaran</th>
                                <th>Biaya</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 12px;">
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->


    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">

    $(document).ready(function() {
        showTable();
        tampilKategori('#KP');
        prosesInBarang();
    });

    function hapusAtributUp() {
        $('input[name=e_kp]').val('');
        $('input[name=e_biaya]').val('');
    }

    function hapusAtributIn() {
        $('input[name=kp]').val('');
        tampilKategori('#KP');
        $('input[name=biaya]').val('');
    }

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/TransaksiPengeluaran/dtTransaksiPengeluaran");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
            "columnDefs": [{ 
                "targets": [0],
                "orderable": false
            }]
        });
     }


    function tampilKategori(ID,id='') {
        $(ID).html('<option value="">-- PILIH KATEGORI --</option>');
        $(ID).select2({
           tags: true
       });

        $.ajax({
            url: '<?=base_url("back/kategori/getKategoriPengeluaran");?>',
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function(data) {
          var val = data.response;
          $.each( val, function( key, value ) {
            if (id != '' && value.id == id) {
              $(ID).append('<option selected value="'+value.id+'">'+value.nama_pengeluaran+'</option>');
            }else{
             $(ID).append('<option  value="'+value.id+'">'+value.nama_pengeluaran+'</option>');
            }
          });
      })
        .fail(function() {
          console.log("error");
      })
        .always(function() {
          console.log("complete");
      });
    }


    function prosesInBarang() {
        $('#formIn').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/TransaksiPengeluaran/prosesInTransPeng");?>',
            type: 'POST',
            dataType : 'JSON',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            hapusAtributIn();
             Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesDeTransPeng(ID) {
       Swal.fire({
          title: 'Apakah anda yakin ingin menghapus data ini ?',
          text: "Anda tidak akan dapat mengembalikan data ini kembali!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak',
        }).then((result) => {
          if (result.value) {
            $.ajax({
                url: '<?=base_url("back/TransaksiPengeluaran/prosesDeTransPeng");?>',
                type: 'POST',
                dataType: 'JSON',
                data : {id : ID}
            })
            .done(function(data) {
                console.log(data.msg);
                showTable();
                Swal.fire(
                    'Terhapus!',
                  data.msg,
                  'success'
                  );
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
          }
        })
    }

    </script>
</body>
</html>
