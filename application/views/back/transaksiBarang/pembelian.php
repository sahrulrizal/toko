 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">

                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Tambah Riwayat Pembelian Barang</div>
                        <div class="ibox-tools">
                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <form id='formIn' method="post" action="javascript:void(0)">
                            <div class="row">
                                <div class="col-md-4 form-group">
                                    <label>Pilih Barang</label><br>
                                    <select style="width: 100%;" required class="form-control" name="barang" id="B">
                                    </select>
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>Jumlah Barang</label><br>
                                    <input class="form-control" required type="number" onkeyup="hitungTotal()" name="jumlah" placeholder="Jumlah barang yang dibeli">
                                </div>
                                <div class="col-sm-4 form-group" id="date_1">
                                    <label>Tanggal Pembelian</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input class="form-control" name="tanggal_transaksi" type="date" value="<?=date('Y-m-d');?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-4 form-group">
                                    <label>Harga Total Barang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon bg-white">Rp.</div>
                                        <input class="form-control" required onkeyup="hitungTotal()" name="harga_total"  type="number">
                                    </div>
                                </div>

                                <div class="col-sm-4 form-group">
                                    <label>Harga Satuan Barang</label>
                                    <div class="input-group">
                                        <div class="input-group-addon bg-white">Rp.</div>
                                        <input class="form-control" name="harga_satuan" readonly type="number">
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Riwayat Transaksi</div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped" id="contoh">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama Barang</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                    <th>Total Harga</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 12px;">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
                <?php $this->load->view($pathFolder.'footer'); ?>
            </div>
        </div>

        <!-- Modal -->
    <div id="edit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
        </div>
        <form id="upForm" action="javascript:void(0)" method="POST">
        <div class="modal-body">
             <div class="row">
                <div class="col-md-4 form-group">
                    <label>Pilih Barang</label><br>
                    <select style="width: 100%;" class="form-control" name="e_sbarang" id="e_B">
                    </select>
                </div>
                <div class="col-md-4 form-group">
                    <label>Jumlah Barang</label><br>
                    <input class="form-control" type="number" onkeyup="hitungTotal()" name="e_jumlah" placeholder="Jumlah barang yang diPembelian">
                </div>
                <div class="col-sm-4 form-group">
                    <label>Harga Satuan Barang</label>
                    <div class="input-group">
                        <div class="input-group-addon bg-white">Rp.</div>
                        <input class="form-control" name="e_harga_satuan" disabled type="number" placeholder="Harga beli per barang (satuan)">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 form-group" id="date_1">
                    <label>Tanggal Pembelian</label>
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input class="form-control" name="e_tanggal_transaksi" type="date" value="04/12/2017">
                    </div>
                </div>
            </div>
        </div>
            <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form> 
        </div>
    </div>

    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">
    $(document).ready(function() {
        showTable();
        prosesInPembelian();
        prosesUpPembelian();
        tampilBarang('#B');
    });

    function hapusAtributUp() {
        $('input[name=e_barang]').val('');
        $('input[name=e_jumlah]').val('');
        $('input[name=e_harga_satuan]').val('');
        $('input[name=e_tanggal_transaksi]').val('');
    }

    function hapusAtributIn() {
        $('input[name=barang]').val('');
        $('input[name=jumlah]').val('');
        $('input[name=harga_satuan]').val('');
        $('input[name=harga_total]').val('');
        $('input[name=tanggal_transaksi]').val('');
    }

    function hitungTotal() {
        var id = $('select[name=barang]').val();
        tampilBarang('','',id);
    }

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/TransaksiBarang/dtPembelian");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
            "columnDefs": [{ 
                "targets": [0],
                "orderable": false
            }]
        });
     }

    function getPembelianID(id) {
        
        hapusAtributUp();

        $.ajax({
            url: '<?=base_url("back/TransaksiBarang/getPembelian?id=");?>'+id,
            type: 'GET',
            dataType : 'json',
        })
        .done(function(data) {

            var val = data.response[0];

            $('input[name=e_id]').val(val.id);
            $('input[name=e_barang]').val(val.id_barang);
            $('input[name=e_jumlah]').val(val.jumlah);
            $('input[name=e_harga_satuan]').val(val.harga_jual);
            $('input[name=e_tanggal_transaksi]').val(val.tanggal_transaksi);

          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
    }

    function tampilBarang(ID,id='',adaID='') {
        $(ID).html('<option value="">-- PILIH BARANG --</option>');
        $(ID).select2();

        var url; 
        if (adaID != '') {
            url = '<?=base_url("back/barang/getBarang?id=");?>'+adaID;

            $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'JSON',
              })
              .done(function(data) {
                var val = data.response[0];
                var total =  parseFloat($('input[name=harga_total]').val());
                var jml = parseFloat($('input[name=jumlah]').val());
                total = parseInt(total/jml);
                $('input[name=harga_satuan]').val(total);
            })
              .fail(function() {
                console.log("error");
            })
              .always(function() {
                console.log("complete");
            });

        }else{
            url = '<?=base_url("back/barang/getBarang");?>';

            $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'JSON',
              })
              .done(function(data) {
                var val = data.response;
                $.each( val, function( key, value ) {
                  if (id != '' && value.id == id) {
                   $(ID).append('<option selected value="'+value.id+'">'+value.nama_barang+'</option>');
                  }else{
                   $(ID).append('<option  value="'+value.id+'">'+value.nama_barang+'</option>');
                  }
                });
            })
              .fail(function() {
                console.log("error");
            })
              .always(function() {
                console.log("complete");
            });
        }
    }


    function prosesInPembelian() {
        $('#formIn').submit(function(event) {
            event.preventDefault();
            Swal.fire({
          title: 'Harap periksa kembali data anda sebelum melanjutkan menyimpan data?',
          text: "Anda tidak akan dapat menghapus atau mengedit transaksi!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak',
        }).then((result) => {
          if (result.value) {
          $.ajax({
              url: '<?=base_url("back/TransaksiBarang/prosesInPembelian");?>',
              type: 'POST',
              dataType : 'JSON',
              data : $( this ).serialize()
          })
          .done(function(data) {
              showTable();
              hapusAtributIn();
               Swal.fire(
                'Sukses!',
                data.msg,
                'success'
                )
               tampilBarang('#B');
               $('input[name=tanggal_transaksi]').val('<?=date('Y-m-d');?>');
            })
              .fail(function() {
                console.log("error");
            })
              .always(function() {
                console.log("complete");
            });
            }
          });
       });
    }

    function prosesUpPembelian() {
        $('#upForm').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/TransaksiBarang/prosesUpPembelian");?>',
            type: 'POST',
            dataType : 'json',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesDePembelian(ID) {
       Swal.fire({
          title: 'Apakah anda yakin ingin menghapus data ini ?',
          text: "Anda tidak akan dapat mengembalikan data ini kembali!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak',
        }).then((result) => {
          if (result.value) {
            $.ajax({
                url: '<?=base_url("back/TransaksiBarang/prosesDePembelian");?>',
                type: 'POST',
                dataType: 'JSON',
                data : {id : ID}
            })
            .done(function(data) {
                console.log(data.msg);
                showTable();
                Swal.fire(
                    'Terhapus!',
                  data.msg,
                  'success'
                  );
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
          }
        })
    }
        </script>
    </body>
</html>
