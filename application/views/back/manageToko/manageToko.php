 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Daftar Toko</div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped" id="contoh">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Toko</th>
                                <th>Nama Akun</th>
                                <th>Username</th>
                                <th>Nomor Telepon</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 12px;">

                        </tbody>
                    </table>
                </div>
            </div>
            </div>

            <!-- END PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>

    <!-- Modal -->
    <div id="edit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
        </div>
        <form id="upForm" action="javascript:void(0)" method="POST">
        <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Nama Toko</label>
                        <input class="form-control"  name="id" type="hidden">
                        <input class="form-control"  name="outlet_name" type="text" placeholder="Nama Outlet">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Nama Akun</label>
                        <input class="form-control" name="nama" type="text" placeholder="Nama Akun">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Email</label>
                        <input class="form-control" name="email" type="email" placeholder="ex: contoh@gmail.com">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Nomor Telepon</label>
                        <input class="form-control" name="no_telp" type="nomor" >
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Username</label>
                        <input class="form-control" name="username" name="username" type="text" placeholder="username">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Password Akun</label>
                        <div class="input-group">
                            <div class="input-group-addon">Ganti Password</div>
                            <input class="form-control" name="password" type="Password" placeholder="******">
                        </div>
                    </div>

                    <div class="col-md-12 form-group">
                        <label>Alamat</label>
                        <textarea class="form-control" name="alamat"></textarea>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form> 
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->


    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">
    $(document).ready(function() {
        showTable();
        prosesUpUsers();
    });

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/Users/dtUsers");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
            "columnDefs": [{ 
                "targets": [0],
                "orderable": false
            }]
        });
    }   

     function getUsers(id='') {
         $.ajax({
            url: '<?=base_url("back/Users/getUsersID?id=");?>'+id,
            type: 'GET',
            dataType : 'json',
        })
        .done(function(data) {

            var val = data;

            $('input[name=id]').val(val.id);
            $('input[name=outlet_name]').val(val.outlet_name);
            $('input[name=nama]').val(val.nama);
            $('input[name=email]').val(val.email);
            $('input[name=username]').val(val.username);
            $('textarea[name=alamat]').text(val.alamat);
            $('input[name=no_telp]').val(val.no_telp);

          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
     }   

     function prosesUpUsers() {
            $('#upForm').submit(function(event) {
                event.preventDefault();
                Swal.fire({
                  title: 'Apakah anda yakin mengubah data ini ?',
                  text: cekGantiPw($('input[name=password]').val()),
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Ya',
                  cancelButtonText: 'Tidak',
              }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '<?=base_url("back/Users/upUsers");?>',
                        type: 'POST',
                        dataType : 'json',
                        data : $( this ).serialize()
                    })
                    .done(function(data) {
                        Swal.fire(
                          'Sukses!',
                          data.msg+cekGantiPw($('input[name=password]').val(),'ya'),
                          'success'
                          )
                        $('input[name=password]').val('');
                        showTable();
                    })
                    .fail(function() {
                      console.log("error");
                  })
                    .always(function() {
                      console.log("complete");
                  });
                }
              });
            });
        }

        function cekGantiPw(v='',$d='') {
            
            var val;

            if ($d == 'ya') {
                 val = ", Password diganti menjadi ("+v+"), mohon untuk di ingat password ini!";
            }else{
                if (v !='') {
                    val = "Password akan diganti menjadi ("+v+"), mohon untuk di ingat password ini!";
                }else{
                    val = '';
                }
            }

            return val;
        }

    </script>
</body>
</html>
