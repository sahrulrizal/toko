 <?php 

 $u = $user->getUsers($user->id_user)->row();

 ?>
 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="ibox">
                            <div class="ibox-body">
                                <ul class="nav nav-tabs tabs-line">
                                    <a><i class="ti-pencil"></i> Update Profile</a>
                                    <hr>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="tab-1">
                                        <form id="upForm" action="javascript:void(0)" method="POST">
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label>Nama Toko</label>
                                                    <input class="form-control" value="<?=$u->outlet_name;?>" <?=$user->cekLevelBoleh()?> name="outlet_name" type="text" placeholder="Nama Outlet">
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label>Nama Akun</label>
                                                    <input class="form-control" name="nama" value="<?=$u->nama;?>" type="text" placeholder="Nama Akun">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label>Email</label>
                                                    <input class="form-control" name="email" value="<?=$u->email;?>" type="email" placeholder="ex: contoh@gmail.com">
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label>Nomor Telepon</label>
                                                    <input class="form-control" name="no_telp" value="<?=$u->no_telp;?>" type="nomor" <?=$user->cekLevelBoleh()?>>
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label>Username</label>
                                                    <input class="form-control" name="username" <?=$user->cekLevelBoleh()?> value="<?=$u->username;?>" name="username" type="text" placeholder="username">
                                                </div>
                                                <div class="col-sm-6 form-group">
                                                    <label>Password Akun</label>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">Ganti Password</div>
                                                        <input class="form-control" name="password" type="Password" placeholder="******">
                                                    </div>
                                                </div>

                                                <div class="col-md-12 form-group">
                                                    <label>Alamat</label>
                                                    <textarea class="form-control" name="alamat"><?=$u->alamat;?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary" type="submit">Simpan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style>
                    .profile-social a {
                        font-size: 16px;
                        margin: 0 10px;
                        color: #999;
                    }

                    .profile-social a:hover {
                        color: #485b6f;
                    }

                    .profile-stat-count {
                        font-size: 22px
                    }
                </style>
            </div>

            <!-- END PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>

    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->

    <!-- BEGIN PAGA BACKDROPS-->
    <!-- END PAGA BACKDROPS-->

    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">

         $(document).ready(function() {
   
        prosesUpProfile();

    });

        function prosesUpProfile() {
            $('#upForm').submit(function(event) {
                event.preventDefault();
                Swal.fire({
                  title: 'Apakah anda yakin mengubah data ini ?',
                  text: cekGantiPw($('input[name=password]').val()),
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Ya',
                  cancelButtonText: 'Tidak',
              }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '<?=base_url("back/Users/upSelfUsers");?>',
                        type: 'POST',
                        dataType : 'json',
                        data : $( this ).serialize()
                    })
                    .done(function(data) {
                        Swal.fire(
                          'Sukses!',
                          data.msg+cekGantiPw($('input[name=password]').val(),'ya'),
                          'success'
                          )
                        $('input[name=password]').val('');
                    })
                    .fail(function() {
                      console.log("error");
                  })
                    .always(function() {
                      console.log("complete");
                  });
                }
              });
            });
        }

        function cekGantiPw(v='',$d='') {
            
            var val;

            if ($d == 'ya') {
                 val = ", Password diganti menjadi ("+v+"), mohon untuk di ingat password ini!";

            }else{
                if (v !='') {
                    val = "Password akan diganti menjadi ("+v+"), mohon untuk di ingat password ini!";
                }else{
                    val = '';
                }
            }

            return val;
        }


      </script>
  </body>
  </html>