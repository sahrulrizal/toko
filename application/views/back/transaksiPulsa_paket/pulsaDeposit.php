 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">

                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Tambah Riwayat Penjualan Pulsa Deposit</div>
                        <div class="ibox-tools">
                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <form action="javascript:;" id="inForm" method="POST">
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Jenis Pulsa Deposit</label>
                                    <select class="form-control" name="jpd" onchange="tampilPP('#PP');" required>
                                        <option value="">-- PILIH --</option>
                                        <option value="1">Pulsa Deposit</option>
                                        <option value="2">Pulsa Deposit Bulk</option>
                                    </select>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Pilihan Pulsa</label><br>
                                    <select style="width: 100%;" onchange="cekHarga()" class="form-control" name="pp" id="PP">
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 form-group">
                                    <label>Harga</label>
                                    <div class="input-group">
                                        <div class="input-group-addon bg-white">Rp.</div>
                                        <input class="form-control" name="harga" type="number" disabled placeholder="Harga">
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Nomor Tujuan</label><br>
                                    <input class="form-control" type="text" name="nomor_tujuan" placeholder="Nomor Tujuan">
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Riwayat Transaksi</div>
                    <div class="ibox-tools">
                        <select class="form-control" name="p_jpd" onchange="showTable()">
                            <option value="1">Pulsa Deposit</option>
                            <option value="2">Pulsa Deposit Bulk</option>
                        </select>
                    </div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped" id="contoh">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Transaksi</th>
                                <th>Jenis Pulsa</th>
                                <th>Pulsa</th>
                                <th>Nomor Tujuan</th>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 12px;">
                            <tr>
                                <td>1</td>
                                <td>1 Maret 2019</td>
                                <td>Pulsa Deposit</td>
                                <td>Tsel 5</td>
                                <td>081212344321</td>
                                <td>Rp. 7000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>

      <!-- Modal -->
    <div id="edit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
        </div>
        <form id="upForm" action="javascript:void(0)" method="POST">
        <div class="modal-body">
         <div class="row">
            <div class="col-sm-6 form-group">
                <label>Jenis Pulsa Deposit</label>
                <select class="form-control" name="e_jpd">
                    <option value="1">Pulsa Deposit</option>
                    <option value="2">Pulsa Deposit Bulk</option>
                </select>
            </div>
            <div class="col-md-6 form-group">
                <label>Pilihan Pulsa</label><br>
                <select style="width: 100%;" class="form-control" name="pp" id="e_PP">
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 form-group">
                <label>Harga</label>
                <div class="input-group">
                    <div class="input-group-addon bg-white">Rp.</div>
                    <input class="form-control" name="e_harga" type="number" disabled placeholder="Harga">
                </div>
            </div>
            <div class="col-md-6 form-group">
                <label>Nomor Tujuan</label><br>
                <input class="form-control" type="text" name="e_nomor_tujuan" placeholder="Nomor Tujuan">
            </div>
        </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form> 
        </div>
    </div>

    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->


    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">
      $(document).ready(function() {
        showTable();
        prosesInPD();
        prosesUpPD();
        // tampilPP('#PP');
    });

    function hapusAtributUp() {
        $('input[name=e_tpdl]').val('');
        $('input[name=e_nomor_tujuan]').val('');
        $('input[name=e_harga]').val('');
        $('input[name=e_tanggal_transaksi]').val('');
    }

    function hapusAtributIn() {
        $('input[name=tpdl]').val('');
        $('input[name=nomor_tujuan]').val('');
        $('input[name=harga]').val('');
        $('input[name=tanggal_transaksi]').val('');
    }

    function cekHarga() {
        var id = $('select[name=pp]').val();
        tampilPP('','',id);
    }

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/TransaksiPulsa_paket/dtPD?id=");?>"+$('select[name=p_jpd]').val(),
            "type": "POST"
        },
        //Set column definition initialisation properties
            "columnDefs": [{ 
                "targets": [0],
                "orderable": false
            }]
        });
     }

    function getPDID(id) {
        
        hapusAtributUp();

        $.ajax({
            url: '<?=base_url("back/TransaksiPulsa_paket/getPD?id=");?>'+id,
            type: 'GET',
            dataType : 'json',
        })
        .done(function(data) {

            var val = data.response[0];

            $('input[name=e_id]').val(val.id);
            $('input[name=e_tpdl]').val(val.id_tpdl);
            $('input[name=e_nomor_tujuan]').val(val.nomor_tujuan);
            $('input[name=e_harga]').val(val.harga_jual);
            $('input[name=e_tanggal_transaksi]').val(val.tanggal_transaksi);

          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
    }

    function tampilPP(ID,id='',adaID='') {
        hapusAtributIn();
        $(ID).html('<option value="">-- PILIH --</option>');
        $(ID).select2();
        var jenis = $('select[name=jpd]').val();

        var url; 
        if (adaID != '') {
            
            if (jenis == 1) {
                url = '<?=base_url("back/Pulsa_paket/getDeposit?id=");?>'+adaID;
            }else if(jenis == 2){
                 url = '<?=base_url("back/Pulsa_paket/getDepositBulk?id=");?>'+adaID;
            }

            $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'JSON',
              })
              .done(function(data) {
                var val = data.response[0];
                $('input[name=harga]').val(val.harga);
            })
              .fail(function() {
                console.log("error");
            })
              .always(function() {
                console.log("complete");
            });

        }else{

            if (jenis == 1) {
                url = '<?=base_url("back/Pulsa_paket/getDeposit");?>';
            }else if(jenis == 2){
                 url = '<?=base_url("back/Pulsa_paket/getDepositBulk");?>';
            }

            $.ajax({
                  url: url,
                  type: 'GET',
                  dataType: 'JSON',
              })
              .done(function(data) {
                var val = data.response;
                $.each( val, function( key, value ) {
                  if (id != '' && value.id == id) {
                   $(ID).append('<option selected value="'+value.id+'">'+value.keterangan+'</option>');
                  }else{
                   $(ID).append('<option  value="'+value.id+'">'+value.keterangan+'</option>');
                  }
                });
            })
              .fail(function() {
                console.log("error");
            })
              .always(function() {
                console.log("complete");
            });
        }
    }


    function prosesInPD() {
        $('#inForm').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/TransaksiPulsa_paket/prosesInPD");?>',
            type: 'POST',
            dataType : 'JSON',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            hapusAtributIn();
             Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesUpPD() {
        $('#upForm').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/TransaksiPulsa_paket/prosesUpPD");?>',
            type: 'POST',
            dataType : 'json',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesDePD(ID) {
       Swal.fire({
          title: 'Apakah anda yakin ingin menghapus data ini ?',
          text: "Anda tidak akan dapat mengembalikan data ini kembali!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak',
        }).then((result) => {
          if (result.value) {
            $.ajax({
                url: '<?=base_url("back/TransaksiPulsa_paket/prosesDePD");?>',
                type: 'POST',
                dataType: 'JSON',
                data : {id : ID}
            })
            .done(function(data) {
                console.log(data.msg);
                showTable();
                Swal.fire(
                    'Terhapus!',
                  data.msg,
                  'success'
                  );
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
          }
        })
    }
    </script>
</body>
</html>
