 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">

                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Tambah Riwayat Service</div>
                        <div class="ibox-tools">
                            <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                            <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                        </div>
                    </div>
                    <div class="ibox-body">
                        <form id='formIn' method="post" action="javascript:void(0)" >
                            <div class="row">
                                <!-- <div class="col-md-4 form-group">
                                    <label>Pilih Outlet</label>
                                    <select class="form-control">
                                        <option value="">Outlet 1</option>
                                        <option value="">Outlet 2</option>
                                        <option value="">Outlet 3</option>
                                        <option value="">Outlet 4</option>
                                    </select>
                                </div> -->
                                <div class="col-md-8 form-group">
                                    <label>Keterangan</label><br>
                                    <input class="form-control" type="text" name="keterangan" placeholder="Isi keterangan service">
                                </div>
                                <div class="col-md-4 form-group">
                                    <label>Biaya</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">Rp.</div>
                                        <input class="form-control" type="number" name="biaya" placeholder="Biaya service">
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Transaksi Service</div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped" id="contoh">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Transaksi</th>
                                <!-- <th>Outlet</th> -->
                                <th>Keterangan</th>
                                <th>Biaya Service</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 12px;">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>

    <!-- Modal -->
    <div id="edit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
        </div>
        <form id="upForm" action="javascript:void(0)" method="POST">
        <div class="modal-body">
                <div class="row">
                      <div class="col-md-8 form-group">
                        <label>Keterangan</label><br>
                        <input class="form-control" type="hidden" name="e_id">
                        <input class="form-control" type="text" name="e_keterangan" placeholder="Isi keterangan service">
                    </div>
                    <div class="col-md-4 form-group">
                        <label>Biaya</label>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input class="form-control" type="number" name="e_biaya" placeholder="Biaya service">
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </div>
            </div>
        </form> 
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->


    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">
    $(document).ready(function() {
        showTable();
        prosesInServicePeng();
        prosesUpServicePeng();
    });

    function hapusAtributUp() {
        $('input[name=e_keterangan]').val('');
        $('input[name=e_biaya]').val('');
    }

    function hapusAtributIn() {
        $('input[name=keterangan]').val('');
        $('input[name=biaya]').val('');
    }

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/TransaksiService/dtTransaksiService");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
            "columnDefs": [{ 
                "targets": [0],
                "orderable": false
            }]
        });
     }

    function getTransaksiServiceID(id) {
        
        hapusAtributUp();

        $.ajax({
            url: '<?=base_url("back/TransaksiService/getTransService?id=");?>'+id,
            type: 'GET',
            dataType : 'json',
        })
        .done(function(data) {

            var val = data.response[0];

            $('input[name=e_id]').val(val.id);
            $('input[name=e_keterangan]').val(val.keterangan);
            $('input[name=e_biaya]').val(val.biaya);

          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
    }


    function prosesInServicePeng() {
        $('#formIn').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/TransaksiService/prosesInTransService");?>',
            type: 'POST',
            dataType : 'JSON',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            hapusAtributIn();
             Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesUpServicePeng() {
        $('#upForm').submit(function(event) {
            event.preventDefault();
             $.ajax({
            url: '<?=base_url("back/TransaksiService/prosesUpTransService");?>',
            type: 'POST',
            dataType : 'json',
            data : $( this ).serialize()
        })
        .done(function(data) {
            showTable();
            Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              )
          })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesDeTransService(ID) {
       Swal.fire({
          title: 'Apakah anda yakin ingin menghapus data ini ?',
          text: "Anda tidak akan dapat mengembalikan data ini kembali!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Ya',
          cancelButtonText: 'Tidak',
        }).then((result) => {
          if (result.value) {
            $.ajax({
                url: '<?=base_url("back/TransaksiService/prosesDeTransService");?>',
                type: 'POST',
                dataType: 'JSON',
                data : {id : ID}
            })
            .done(function(data) {
                console.log(data.msg);
                showTable();
                Swal.fire(
                    'Terhapus!',
                  data.msg,
                  'success'
                  );
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
          }
        })
    }

    </script>
</body>
</html>
