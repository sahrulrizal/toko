 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <style type="text/css">
     .form-control{
        font-size: 12px;
     }
 </style>
 <body class="fixed-navbar">
  <div class="page-wrapper">
    <!-- START HEADER-->
    <?php $this->load->view($pathFolder.'header'); ?>
    <!-- END HEADER-->
    <!-- START SIDEBAR-->
    <?php $this->load->view($pathFolder.'sidebar'); ?>
    <!-- END SIDEBAR-->
    <div class="content-wrapper">
      <!-- START PAGE CONTENT-->
      <?php $this->load->view($pathFolder.'breadcrumb'); ?>
      <div class="page-content fade-in-up">

        <div class="ibox">
          <div class="ibox-head">
            <div class="ibox-title">Optional</div>
            <div class="ibox-tools">
              <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
              <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
          </div>
          <form action="javascript:void(0)" method="get" id="cariForm">
          <div class="ibox-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                <label>Pilih Toko</label>
                <select style="width: 100%;" required class="form-control"  name="toko" id="PT">
                </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                <label>Pilih Bulan</label>
                  <input type="month" class="form-control" name="bulan" value=" <?=date('M Y')?>">
                </div>
              </div>
            </div>
          </div>
          <div class="ibox-footer" style="text-align: right;margin-right: 10px;" >
            <input type="submit" name="btn" value="Cari" class="btn btn-primary">
          </div>
          </form>
        </div>
      </div>

      <div class="ibox">
        <div class="ibox-head">
          <div class="ibox-title">Riwayat Diskon <span id="txtToko"></span></div>
        </div>
        <div class="ibox-body">
          <table class="table table-striped" id="contoh">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal Transaksi</th>
                <th>Kode Struk</th>
                <th>Diskon</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody style="font-size: 12px;">
             
            </tbody>
          </table>
        </div>
      </div>

      <!-- MODEL -->

      <div id="view" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h5 class="modal-title">Detail <span id="e_struk"></span></h5>
            </div>
            <div class="modal-body">
              <table class="table table-responseive" id="list" style="font-size: 11px;">
                <thead>
                  <tr>
                    <th>Nama Barang</th>
                    <th>Jumlah</th>
                    <th>Diskon</th>
                    <th>Harga Satuan</th>
                    <th>Total Harga</th>
                  </tr>
                </thead>
                <tbody style="font-size: 12px;" id="e_list_body">
                  <tr class="list_vars">
                    <td style="width: 250px;"> 
                      <span id="e_nama_barang">-</span>
                    </td>
                    <td style="width: 110px;">
                       <span id="e_jumlah">-</span>
                    </td>
                    <td style="width: 110px;">
                       <span id="e_diskon">-</span>
                    </td>
                    <td> 
                       <span id="e_harga_satuan">-</span>
                    </td>
                    <td>
                       <span id="e_total_harga">-</span>
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="4"><b>Total Keseluruhan</b></td>
                    <td><span id="e_total_all"></span></td>
                  </tr>
                </tfoot>
              </table> 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>


      <!-- BEGIN THEME CONFIG PANEL-->

      <!-- END THEME CONFIG PANEL-->


      <!-- CORE PLUGINS-->
      <?php $this->load->view($pathFolder.'script'); ?>

      <script>  
          

      $('#list').addInputArea({
        area_del: '.del-area'
      });

      $(function() {
       var arr = [];
        $('#add').click(function(ev) {
            ev.preventDefault();
            var ok = form('.form-serialize');
            var ok1 = ok.split('list-barang_');
            var ok2 = ok1.length;
            var c = ok1.length-1;
            if ((ok1.length-1) == 1) {
              c = 0;
            }
            arr.push(parseInt(c)-1);
            setTimeout(function() {
              console.log(arr);
                for (var i = 0; i < ok2; i++) {
                    if (arr[i] != i && i != 0) {
                      tampilToko('select[name=list-barang_'+i+']');   
                    }
                   console.log(arr[i]);
                }
            ubahId();
            }, 500);

        });
      });
       
       $(document).ready(function() {
        showTable();
        cariForm();
        tampilToko('#PT');
        $('#simpan').attr('disabled','');

        ubahId();
      });

       $('#hapus').click(function(event) {
        event.preventDefault();
        ubahId();
       });

       function ubahId() {
          $('.id*').attr('name','id[]');
          $('.struk').attr('name','struk');
          $('.total_all').attr('name','total_all');
       }

       function pilihBarang(v) {
           // alert(v.value);
           var node = parseFloat(v.attributes.name.nodeValue.split("_")[1]);
           tampilToko('','',v.value,node);
       }

       function tamplTotalSemua(v) {
         $('#total_all').html(v);
       }

      function hitungTotal(v) {
        var node = parseFloat(v.attributes.name.nodeValue.split("_")[1]);
        var id = $('select[name=list-barang_'+node+']').val();
        tampilToko('','',id,node);

        var ok = form('.form-serialize');
        var ok1 = ok.split('list-total_harga_');
        var ok2 = ok1.length;
        var t = parseInt(0);
        var total_semua = [];

        setTimeout(function() {
        for (var i = 0; i < ok2; i++) {
            var total = parseFloat($('input[name=list-total_harga_'+i+']').val());
            if (!isNaN(total)) {  
              t+= parseInt(total);
              total_semua.push({t}); 
            }
          }
          $('#total_all').html('Rp. '+t);
          $('.total_all').val(t);
          $('#simpan').removeAttr('disabled');
         }, 500);
        tamplTotalSemua(t);
      }

      function hapus(v) {
        var node = parseFloat(v.attributes.name.nodeValue.split("_")[1]);
        setTimeout(function() {
          ubahId();
        },200)
      }

       function hapusAtributUp() {
        $('input[name=e_barang]').val('');
        $('input[name=e_jumlah]').val('');
        $('input[name=e_harga_satuan]').val('');
        $('input[name=e_tanggal_transaksi]').val('');
      }

      function form(v='') {
            var arr = $(v).serializeArray();
            var result = '';
            $.each(arr, function(i, f) {
              result += f.name + ': ' + f.value + '\n';
            });
            return result;
      }

      function hapusAtributIn() {
        $('input[name=barang]').val('');
        $('input[name=jumlah]').val('');
        $('input[name=harga_satuan]').val('');
        $('input[name=tanggal_transaksi]').val('');
      }

      function showTable(pToko='',pBulan='') {
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
          "url": "<?=base_url('back/Diskon/dtDiskon');?>?id_user="+pToko+"&bulan="+pBulan,
          "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{ 
          "targets": [0],
          "orderable": false
        }]
      });
       }

       function getPenjualanID(kode_struk) {
        
        hapusAtributUp();
        $('#e_list_body').html('');

        $.ajax({
          url: '<?=base_url("back/TransaksiBarang/getPenjualan?kode_struk=");?>'+kode_struk,
          type: 'GET',
          dataType : 'json',
        })
        .done(function(data) {
          var val = data.response;
          var ttl = parseInt(0);

          $.each(val, function( key, value ) {
              $('#e_list_body').append('<tr class="list_var"> <td style="width: 250px;"> <span id="e_nama_barang">'+value.nama_barang+'</span> </td> <td style="width: 110px;"> <span id="e_jumlah">'+value.jumlah+'</span> </td> <td style="width: 110px;"> <span id="e_diskon">Rp.'+value.diskon+'</span> </td> <td> <span id="e_harga_satuan">Rp.'+value.harga_satuan+'</span> </td> <td> <span id="e_total_harga">Rp.'+value.total_harga+'</span></td></tr>'); 
              ttl += parseInt(value.total_harga);
          });
          
          $('#e_struk').html('#'+kode_struk);  
          setTimeout(function(){ 
            $('#e_total_all').html('<b>Rp. '+ttl+'</b>');
          }, 150);
          // $('input[name=e_id]').valueal(val.id);
          // $('input[name=e_barang]').val(val.id_barang);
          // $('input[name=e_jumlah]').val(val.jumlah);
          // $('input[name=e_harga_satuan]').val(val.harga_jual);
          // $('input[name=e_tanggal_transaksi]').val(val.tanggal_transaksi);

        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });
      }

      function tampilToko(ID,id='',adaID='',node='') {
        // $(ID).html('<option value="">-- PILIH BARANG --</option>');
        $(ID).select2();

        var url; 
        if (adaID != '') {
          url = '<?=base_url("back/BarangOutlet/getBarang?id=");?>'+adaID;

          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
          })
          .done(function(data) {
            var val = data.response[0];
            $('input[name=list-total_harga_'+node+']').val(val.harga_jual * $('input[name=list-jumlah_'+node+']').val());
            $('input[name=list-harga_satuan_'+node+']').val(val.harga_jual);
            $('input[name=list-total_harga_'+node+']').val($('input[name=list-total_harga_'+node+']').val() - $('input[name=list-diskon_'+node+']').val());

            if (val.stok <= 0) {
             $('input[name=list-jumlah_'+node+']').attr('disabled', '');
            
             Swal.fire(
              'Maaf!',
              'Stok barang tersebut sudah kosong, harap pilih barang yang lainnya!',
              'error'
              );

            }else if (parseInt($('input[name=list-jumlah_'+node+']').val()) > val.stok || parseInt($('input[name=list-jumlah_'+node+']').val()) == 0) {
             $('input[name=list-jumlah_'+node+']').val('');

              Swal.fire(
              'Maaf!',
              'Stok barang tersebut saat ini adalah '+val.stok+', mohon untuk tidak memasukan jumlah stok melebihi stok atau kurang dari stok yang ada!',
              'error'
              );

            }else{
              $('input[name=list-jumlah_'+node+']').removeAttr('disabled');
            }

          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

        }else{
          url = '<?=base_url("back/Laporan/getToko");?>';

          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
          })
          .done(function(data) {
            var val = data;
            $.each( val, function( key, value ) {
              if (id != '' && value.id == id) {
               $(ID).append('<option selected value="'+value.id+'">'+value.outlet_name+'</option>');
             }else{
               $(ID).append('<option  value="'+value.id+'">'+value.outlet_name+'</option>');
             }
           });
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

          //   $(ID).select2({
          //      minimumInputLength: 2,
          //      allowClear: true,
          //      placeholder: '-- PILIH BARANG --',
          //      ajax: {
          //         dataType: 'json',
          //         url: url,
          //         delay: 800,
          //         data: function(params) {
          //           return {
          //             search: params.term
          //           }
          //         },
          //         processResults: function (data, page) {
          //         return {
          //           results: data
          //         };
          //       },
          //     }
          // }).on('select2:select', function (evt) {
          //    var data = $(ID+" option:selected").text();
          // });

        }
      }


      function prosesInPenjualan() {
        $('#inForm').submit(function(event) {
          event.preventDefault();
          $.ajax({
            url: '<?=base_url("back/TransaksiBarang/prosesInPenjualan");?>',
            type: 'POST',
            dataType : 'JSON',
            data : $( this ).serialize()
          })
          .done(function(data) {
            showTable();
            hapusAtributIn();
            Swal.fire(
              'Sukses!',
              data.msg,
              'success'
              );
            $('#list_body').html('<tr class="list_var"> <td style="width: 250px;"> <select style="width: 100%;" required class="form-control pb" onchange="pilihBarang(this)"  name="list-barang_0" id="PB"> <option>loading..</option> </select> </td> <td style="width: 110px;"> <input type="hidden" name="id[]" class="id"> <input type="hidden" name="struk" class="struk" readonly value="<?= random_string('alnum',8) ?>"> <input class="form-control" type="number" required onchange="hitungTotal(this)" name="list-jumlah_0" placeholder="Jumlah"> </td> <td> <div class="input-group"> <div class="input-group-addon bg-white">Rp.</div> <input class="form-control" type="number" onchange="hitungTotal(this)" name="list-diskon_0" placeholder="Diskon"> </div> </td> <td> <div class="input-group"> <div class="input-group-addon bg-white">Rp.</div> <input class="form-control" readonly name="list-harga_satuan_0" type="number" placeholder=""> </div></td> <td> <div class="input-group"> <div class="input-group-addon bg-white">Rp.</div> <input class="form-control" readonly name="list-total_harga_0" type="number" placeholder=""> </div></td> </tr>');
            tampilToko('#PT');
            $('#total_all').html('');
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });
        });
      }

      function cariForm() {
        $('#cariForm').submit(function(event) {
        var bulan = $('input[name=bulan]');
        var toko = $('select[name=toko]');
        $('#txtToko').text('- '+toko.text()+' ('+bulan.val()+')' );
          showTable(toko.val(),bulan.val());
        });
      }

      
  </script>

</body>
</html>
