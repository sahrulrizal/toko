 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">

              <div class="ibox">
                <div class="ibox-head">
                  <div class="ibox-title">Optional</div>
                  <div class="ibox-tools">
                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                    <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                  </div>
                </div>
                <form action="javascript:void(0)" method="get" id="cariForm">
                  <div class="ibox-body">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Pilih Toko</label>
                          <select style="width: 100%;" required class="form-control"  name="toko" id="PT">
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>Pilih Bulan</label>
                          <input type="month" class="form-control" name="bulan" value=" <?=date('M Y')?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="ibox-footer" style="text-align: right;margin-right: 10px;" >
                    <input type="submit" name="btn" value="Cari" class="btn btn-primary">
                  </div>
                </form>
              </div>

                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Riwayat Transaksi <span id="txtToko"></span></div>
                    </div>
                    <div class="ibox-body">
                        <table class="table table-striped" id="contoh">
                            <thead>
                              <tr>
                                 <th>No</th>
                                <th>Tanggal Transaksi</th>
                                <th>Pilihan Pulsa</th>
                                <th>Nomor Tujuan</th>
                                <th>Harga</th>
                             </tr>
                            </thead>
                            <tbody style="font-size: 12px;">
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
                <?php $this->load->view($pathFolder.'footer'); ?>
            </div>
        </div>

    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">

      $(document).ready(function() {
        showTable();
        cariForm();
        tampilToko('#PT');
      });

      function showTable(pToko='',pBulan='') {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
          "url": "<?=base_url("back/TransaksiPulsa_paket/dtPTDL");?>?id_user="+pToko+"&bulan="+pBulan,
          "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{ 
          "targets": [0],
          "orderable": false
        }]
      });
       }


       function tampilToko(ID,id='',adaID='') {
        $(ID).select2();

        var url; 
        if (adaID != '') {
          url = '<?=base_url("back/barang/getBarang?id=");?>'+adaID;

          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
          })
          .done(function(data) {
            var val = data.response[0];
            var total =  parseFloat($('input[name=harga_total]').val());
            var jml = parseFloat($('input[name=jumlah]').val());
            total = parseInt(total/jml);
            $('input[name=harga_satuan]').val(total);
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });

        }else{

          url = '<?=base_url("back/Laporan/getToko");?>';

          $.ajax({
            url: url,
            type: 'GET',
            dataType: 'JSON',
          })
          .done(function(data) {
            var val = data;
            $.each( val, function( key, value ) {
              if (id != '' && value.id == id) {
               $(ID).append('<option selected value="'+value.id+'">'+value.outlet_name+'</option>');
             }else{
               $(ID).append('<option  value="'+value.id+'">'+value.outlet_name+'</option>');
             }
           });
          })
          .fail(function() {
            console.log("error");
          })
          .always(function() {
            console.log("complete");
          });
        }
      }

      function cariForm() {
        $('#cariForm').submit(function(event) {
          var bulan = $('input[name=bulan]');
          var toko = $('select[name=toko]');
          $('#txtToko').text('- '+toko.text()+' ('+bulan.val()+')' );
          showTable(toko.val(),bulan.val());
        });
      }

    </script>
    </body>
</html>
