 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">
                <div class="row">
                    <?php foreach ($user->getUsers('','',['level' => '1'])->result() as $toko): ?>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-success color-white widget-stat">
                            <div class="ibox-body">
                                <h5 class="m-b-5 font-strong"><?=$dm->pendapatan($toko->id)['uangMasuk']?></h5>
                                <div class="m-b-5">TOTAL PENDAPATAN <br><?=$toko->outlet_name;?></div><i class="ti-wallet widget-stat-icon"></i>
                                <div><i class="fa fa-calendar m-r-5"></i><small>Update <?=date('d M Y')?></small></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>

                    <!-- <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-warning color-white widget-stat">
                            <div class="ibox-body">
                                <h5 class="m-b-5 font-strong">Rp. 31.000.000</h5>
                                <div class="m-b-5">TOTAL PENDAPATAN <br>Outlet Dusanak</div><i class="ti-wallet widget-stat-icon"></i>
                                <div><i class="fa fa-calendar m-r-5"></i><small>Update 22 April 2019</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-info color-white widget-stat">
                            <div class="ibox-body">
                                <h5 class="m-b-5 font-strong">Rp. 35.000.000</h5>
                                <div class="m-b-5">TOTAL PENDAPATAN <br>Outlet Rancak</div><i class="ti-wallet widget-stat-icon"></i>
                                <div><i class="fa fa-calendar m-r-5"></i><small>Update 22 April 2019</small></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-danger color-white widget-stat">
                            <div class="ibox-body">
                                <h5 class="m-b-5 font-strong">Rp. 13.000.000</h5>
                                <div class="m-b-5">TOTAL PENDAPATAN <br>Outlet PSMS</div><i class="ti-wallet widget-stat-icon"></i>
                                <div><i class="fa fa-calendar m-r-5"></i><small>Update 22 April 2019</small></div>
                            </div>
                        </div>
                    </div> -->

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="flexbox mb-4">
                                    <div>
                                        <h3 class="m-0">Statistik</h3>
                                        <div>Pendapatan Tahun 
                                            <select name="tahun" onchange="ubahTahun()">
                                                <?php for ($i=19; $i <= 30; $i++) {  ?>
                                                <option <?=date('Y') == '20'.$i ? 'selected' : '';?> value="20<?=$i?>">20<?=$i?></option>
                                               <?php } ?>
                                            </select>
                                         (Per Bulan)</div>
                                    </div>
                                    <div class="d-inline-flex">
                                        <div class="px-3">
                                            <div class="text-muted">Total Pendapatan Seluruh Outlet Tahun Ini</div>
                                            <div>
                                                <span class="h2 m-0" id="totalSeluruh">-</span>
                                                <span class="text-success ml-2"><i class="fa fa-money"></i> <?=date('M, Y')?></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                <canvas id="bar_chart" style="height: 300px; display: block; width: 637px;" width="637" height="260" class="chartjs-render-monitor"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($user->getUsers('','',['level' => '1'])->result() as $toko){ ?>
                    <div class="col-lg-3">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Laporan <?=$toko->outlet_name;?></div>
                            </div>
                            <div class="ibox-body">
                                <input type="month" onchange="ubahTanggal(<?=$toko->id?>)" name="bulan" class="form-control" value="<?=date('Y-m')?>">
                                <br>
                                <div class="row align-items-center">
                                    <div class="col-md-12">
                                        <div class="m-b-10 text-success"><i class="fa fa-caret-up m-r-10"></i>Uang Masuk :<br> <span id="uangMasuk"><?=$dm->pendapatan($toko->id)['uangMasuk']?></span></div>
                                        <div class="m-b-10 text-danger"><i class="fa fa-caret-down m-r-10"></i>Uang Keluar :<br> <span id="uangKeluar"><?=$dm->pendapatan($toko->id)['uangKeluar']?></span></div>
                                    </div>
                                </div>
                                <ul class="list-group list-group-divider list-group-full">
                                    <li class="list-group-item">Penjualan
                                        <span class="float-right text-success" id="penjualan"><?=$dm->pendapatan($toko->id)['penjualan']?></span>
                                    </li>
                                    <li class="list-group-item">Pembelian
                                        <span class="float-right text-warning" id="pembelian"><?=$dm->pendapatan($toko->id)['pembelian']?></span>
                                    </li>
                                    <li class="list-group-item">Retur
                                        <span class="float-right text-success" id="retur"><?=$dm->pendapatan($toko->id)['retur']?></span>
                                    </li>
                                    <li class="list-group-item">Diskon
                                        <span class="float-right text-warning" id="diskon"><?=$dm->pendapatan($toko->id)['diskon']?></span>
                                    </li>
                                    <li class="list-group-item">Service
                                        <span class="float-right text-success" id="service"><?=$dm->pendapatan($toko->id)['service']?></span>
                                    </li>
                                    <li class="list-group-item">PTL
                                        <span class="float-right text-success" id="ptl"><?=$dm->pendapatan($toko->id)['ptl']?></span>
                                    </li>
                                    <li class="list-group-item">PIU
                                        <span class="float-right text-success" id="piu"><?=$dm->pendapatan($toko->id)['vPIU']?></span>
                                    </li>
                                    <li class="list-group-item">Deposit
                                        <span class="float-right text-success" id="deposit"><?=$dm->pendapatan($toko->id)['vdeposit']?></span>
                                    </li>
                                    <li class="list-group-item">Pengeluaran
                                        <span class="float-right text-warning" id="pengeluaran"><?=$dm->pendapatan($toko->id)['pengeluaran']?></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>

            </div>
     <!--    <div class="row">
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Top 5 Best Sellers Product</div>
                    </div>
                    <div class="ibox-body">
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">Batere Samsung
                                <span class="float-right text-primary">97 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">KABEL USB HUB
                                <span class="float-right text-primary">77 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">HP Samsung J4
                                <span class="float-right text-primary">67 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">HP Second
                                <span class="float-right text-primary">50 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">Sabun Cuci Piring
                                <span class="float-right text-primary">35 Pcs</span>
                            </li>
                        </ul>
                    </div>
                    <div class="ibox-footer text-center">
                        <a href="javascript:;">View All Products</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Top 5 Best Sellers Product</div>
                    </div>
                    <div class="ibox-body">
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">Batere Samsung
                                <span class="float-right text-primary">97 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">KABEL USB HUB
                                <span class="float-right text-primary">77 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">HP Samsung J4
                                <span class="float-right text-primary">67 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">HP Second
                                <span class="float-right text-primary">50 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">Sabun Cuci Piring
                                <span class="float-right text-primary">35 Pcs</span>
                            </li>
                        </ul>
                    </div>
                    <div class="ibox-footer text-center">
                        <a href="javascript:;">View All Products</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Top 5 Best Sellers Product</div>
                    </div>
                    <div class="ibox-body">
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">Batere Samsung
                                <span class="float-right text-primary">97 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">KABEL USB HUB
                                <span class="float-right text-primary">77 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">HP Samsung J4
                                <span class="float-right text-primary">67 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">HP Second
                                <span class="float-right text-primary">50 Pcs</span>
                            </li>
                        </ul>
                        <ul class="list-group list-group-divider list-group-full">
                            <li class="list-group-item">Sabun Cuci Piring
                                <span class="float-right text-primary">35 Pcs</span>
                            </li>
                        </ul>
                    </div>
                    <div class="ibox-footer text-center">
                        <a href="javascript:;">View All Products</a>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <!-- END PAGE CONTENT-->
    <?php $this->load->view($pathFolder.'footer'); ?>
</div>
</div>

<!-- CORE PLUGINS-->
<?php $this->load->view($pathFolder.'script'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            statistik();
        });       
        
        function ubahTanggal(id='') {
            $.ajax({
                url: '<?=base_url("back/Dashboard/pendapatan");?>?id='+id+'&bulan='+$('input[name=bulan]').val(),
                type: 'GET',
                dataType : 'JSON',
            })
            .done(function(data) {
                $('#uangMasuk').text(data.uangMasuk);
                $('#uangKeluar').text(data.uangKeluar);
                $('#penjualan').text(data.penjualan);
                $('#pembelian').text(data.pembelian);
                $('#diskon').text(data.diskon);
                $('#retur').text(data.retur);
                $('#service').text(data.service);
                $('#ptl').text(data.ptl);
                $('#piu').text(data.vPIU);
                $('#deposit').text(data.vdeposit);
                $('#pengeluaran').text(data.pengeluaran);
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });
        }

        function ubahTahun() {
            statistik($('select[name=tahun]').val());
        }

        function statistik(tahun='') {
           
            if (tahun == '') {
               tahun = $('select[name=tahun]').val();
            }

            $.ajax({
                url: '<?=base_url("back/Dashboard/statistik");?>?tahun='+tahun,
                type: 'GET',
                dataType : 'JSON',
            })
               .done(function(bar) {

                $('#totalSeluruh').text(bar.totalSeluruh);

                var barOptions = {
                    responsive: true,
                    maintainAspectRatio: false
                };

                var ctx = document.getElementById("bar_chart").getContext("2d");
                new Chart(ctx, {type: 'bar', data: bar, options:barOptions}); 
            })
               .fail(function() {
                console.log("error");
            })
               .always(function() {
                console.log("complete");
            });
        }

    </script>

</body>
</html>