 <!-- head -->
 <?php $this->load->view($pathFolder.'head'); ?>
 <body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-md-12">

                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Tambah Barang</div>
                                <div class="ibox-tools">
                                    <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                    <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item">option 1</a>
                                        <a class="dropdown-item">option 2</a>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-body">
                                <form id="formBarang" action="javascript:void(0)" method="POST">
                                    <div class="row">
                                        <div class="col-sm-6 form-group">
                                            <label>Nama Barang</label>
                                            <input class="form-control" required type="text" name="nama_barang" placeholder="Nama barang">
                                        </div>
                                        <div class="col-sm-6  form-group">
                                            <label>Kode Barang</label>
                                            <input class="form-control" type="text" name="kode_barang" placeholder="Kode barang">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>Kategori Barang</label><br>
                                            <select style="width: 100%;" class="form-control"  name="kategori" id="KT">
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Nama Supplier</label><br>
                                            <select style="width: 100%;" class="form-control"  name="nama_supplier" id="NS">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 form-group">
                                            <label>Harga Beli</label>
                                            <div class="input-group">
                                                <div class="input-group-addon">Rp.</div>
                                                <input class="form-control" name="harga_beli" type="number" placeholder="Harga beli barang">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 form-group">
                                            <label>Harga Jual</label>

                                            <div class="input-group">
                                                <div class="input-group-addon">Rp.</div>
                                                <input class="form-control" name="harga_jual" type="number" placeholder="Harga jual barang">
                                            </div>
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label>Stok Awal</label><br>
                                            <input class="form-control" name="jumlah_stok" type="number" placeholder="Stok awal">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Data Barang</div>
                            </div>
                            <div class="ibox-body">
                                <table class="table table-striped" id="contoh">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Kode Barang</th>
                                            <!-- <th>Kategori</th> -->
                                            <!-- <th>Nama Supplier</th> -->
                                            <th>Harga Beli</th>
                                            <th>Harga Jual</th>
                                            <th>Jumlah Stok</th>
                                            <th>Tanggal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 12px;">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>

    <!-- Modal -->
    <div id="edit" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit</h4>
        </div>
        <form id="formEditBarang" action="javascript:void(0)" method="POST">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Nama Barang</label>
                        <input class="form-control"  type="hidden" name="e_id">
                        <input class="form-control" required type="text" name="e_nama_barang" placeholder="Nama barang">
                    </div>
                    <div class="col-sm-6  form-group">
                        <label>Kode Barang</label>
                        <input class="form-control" type="text" name="e_kode_barang" placeholder="Kode barang">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 form-group">
                        <label>Kategori Barang</label><br>
                        <select style="width: 100%;" class="form-control"  name="e_kategori" id="KT">
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label>Nama Supplier</label><br>
                        <select style="width: 100%;" class="form-control"  name="e_nama_supplier" id="NS">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label>Harga Beli</label>
                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input class="form-control" name="e_harga_beli" type="number" placeholder="Harga beli barang">
                        </div>
                    </div>
                    <div class="col-sm-6 form-group">
                        <label>Harga Jual</label>

                        <div class="input-group">
                            <div class="input-group-addon">Rp.</div>
                            <input class="form-control" name="e_harga_jual" type="number" placeholder="Harga jual barang">
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12 form-group">
                        <label>Stok</label><br>
                        <input class="form-control" name="e_jumlah_stok" type="number" placeholder="Stok">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" type="submit">Simpan</button>
            </div>
        </div>
        </form> 
    </div>
    </div>

<!-- BEGIN THEME CONFIG PANEL-->

<!-- END THEME CONFIG PANEL-->

<!-- BEGIN PAGA BACKDROPS-->
<!-- END PAGA BACKDROPS-->

<!-- CORE PLUGINS-->
<?php $this->load->view($pathFolder.'script'); ?>
<script type="text/javascript">

    $(document).ready(function() {
        showTable();
        tampilSupplier('#NS');
        tampilKategori('#KT');
        prosesInBarang();
        prosesUpBarang();

    });

    function hapusAtributUp() {
        $('input[name=e_id]').val('');
        $('input[name=e_nama_barang]').val('');
        $('input[name=e_kode_barang]').val('');
        $('select[name=e_kategori]').html('');
        $('select[name=e_nama_supplier]').html('');
        $('input[name=e_harga_beli]').val('');
        $('input[name=e_harga_jual]').val('');
        $('input[name=e_jumlah_stok]').val('');
    }

    function hapusAtributIn() {
        $('input[name=nama_barang]').val('');
        $('input[name=kode_barang]').val('');
        tampilSupplier('#NS');
        tampilKategori('#KT');
        $('input[name=harga_beli]').val('');
        $('input[name=harga_jual]').val('');
        $('input[name=jumlah_stok]').val('');
    }

    function showTable() {
         // body...
         $('#contoh').DataTable({
        // Processing indicator
        "destroy": true,
        "searching": true,
        "processing": true,
        // DataTables server-side processing mode
        "serverSide": true,
        "scrollX": true,
        // Initial no order.
        "order": [],
        // Load data from an Ajax source
        "ajax": {
            "url": "<?=base_url("back/barang/dtBarang");?>",
            "type": "POST"
        },
        //Set column definition initialisation properties
        "columnDefs": [{ 
            "targets": [0],
            "orderable": false
        }]
    });
     }

     function tampilSupplier(ID,id = '') {
        $(ID).html('<option value="">-- PILIH SUPPLIER --</option>');
        $(ID).select2({
         tags: true
     });

        $.ajax({
            url: '<?=base_url("back/supplier/getSupplier");?>',
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function(data) {
          var val = data.response;
          $.each( val, function( key, value ) {
            if (id != '' && value.id == id) {
              $(ID).append('<option selected value="'+value.id+'">'+value.supplier+'</option>');
          }else{
              $(ID).append('<option value="'+value.id+'">'+value.supplier+'</option>');
          }
      });
      })
        .fail(function() {
          console.log("error");
      })
        .always(function() {
          console.log("complete");
      });
    }

    function tampilKategori(ID,id='') {
        $(ID).html('<option value="">-- PILIH KATEGORI --</option>');
        $(ID).select2({
         tags: true
     });

        $.ajax({
            url: '<?=base_url("back/kategori/getKategoriBarang");?>',
            type: 'GET',
            dataType: 'JSON',
        })
        .done(function(data) {
          var val = data.response;
          $.each( val, function( key, value ) {
            if (id != '' && value.id == id) {
              $(ID).append('<option selected value="'+value.id+'">'+value.kategori+'</option>');
          }else{
           $(ID).append('<option  value="'+value.id+'">'+value.kategori+'</option>');
       }
   });
      })
        .fail(function() {
          console.log("error");
      })
        .always(function() {
          console.log("complete");
      });
    }

    function getBarangID(id) {
        
        hapusAtributUp();

        $.ajax({
            url: '<?=base_url("back/barang/getBarang?id=");?>'+id,
            type: 'GET',
            dataType : 'json',
        })
        .done(function(data) {

            var val = data.response[0];

            $('input[name=e_id]').val(val.id);
            $('input[name=e_nama_barang]').val(val.nama_barang);
            $('input[name=e_kode_barang]').val(val.kode);
            $('select[name=e_kategori]').val(val.kategori);

            tampilKategori("select[name=e_kategori]",val.kategori);
            tampilSupplier("select[name=e_nama_supplier]",val.supplier);

            $('select[name=e_nama_supplier]').val(val.supplier);
            $('input[name=e_harga_beli]').val(val.harga_beli);
            $('input[name=e_harga_jual]').val(val.harga_jual);
            $('input[name=e_jumlah_stok]').val(val.stok);

        })
        .fail(function() {
          console.log("error");
      })
        .always(function() {
          console.log("complete");
      });
    }


    function prosesInBarang() {
        $('#formBarang').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: '<?=base_url("back/barang/prosesInBarang");?>',
                type: 'POST',
                dataType : 'JSON',
                data : $( this ).serialize()
            })
            .done(function(data) {
                showTable();
                hapusAtributIn();
                Swal.fire(
                  'Sukses!',
                  data.msg,
                  'success'
                  )
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }
    
    function prosesUpBarang() {
        $('#formEditBarang').submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: '<?=base_url("back/barang/prosesUpBarang");?>',
                type: 'POST',
                dataType : 'json',
                data : $( this ).serialize()
            })
            .done(function(data) {
                showTable();
                Swal.fire(
                  'Sukses!',
                  data.msg,
                  'success'
                  )
            })
            .fail(function() {
              console.log("error");
          })
            .always(function() {
              console.log("complete");
          });
        });
    }

    function prosesDeBarang(ID) {
     Swal.fire({
      title: 'Apakah anda yakin ingin menghapus data ini ?',
      text: "Anda tidak akan dapat mengembalikan data ini kembali!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
  }).then((result) => {
      if (result.value) {
        $.ajax({
            url: '<?=base_url("back/barang/prosesDeBarang");?>',
            type: 'POST',
            dataType: 'JSON',
            data : {id : ID}
        })
        .done(function(data) {
            console.log(data.msg);
            showTable();
            Swal.fire(
                'Terhapus!',
                data.msg,
                'success'
                );
        })
        .fail(function() {
          console.log("error");
      })
        .always(function() {
          console.log("complete");
      });
    }
})
}

</script>
</body>
</html>