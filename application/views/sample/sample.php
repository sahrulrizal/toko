 <!-- head -->
<?php $this->load->view($pathFolder.'head'); ?>
<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <?php $this->load->view($pathFolder.'header'); ?>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
       <?php $this->load->view($pathFolder.'sidebar'); ?>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <?php $this->load->view($pathFolder.'breadcrumb'); ?>
            <div class="page-content fade-in-up">
                <!-- Isi disini bro -->
            </div>
            <!-- END PAGE CONTENT-->
           <?php $this->load->view($pathFolder.'footer'); ?>
        </div>
    </div>
    
    <!-- BEGIN THEME CONFIG PANEL-->
   
    <!-- END THEME CONFIG PANEL-->
    
    <!-- BEGIN PAGA BACKDROPS-->
    <?php $this->load->view($pathFolder.'loading'); ?>
    <!-- END PAGA BACKDROPS-->

    <!-- CORE PLUGINS-->
    <?php $this->load->view($pathFolder.'script'); ?>
</body>
</html>