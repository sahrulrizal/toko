<header class="header">
            <div class="page-brand">
                <a class="link" href="index.html">
                    <span class="brand"><?= $n = $user->getUsers($user->id_user)->row()->outlet_name;?>
                        <!-- <span class="brand-tip">Ponsel</span> -->
                    </span>
                    <span class="brand-mini">
                        <?php 
                            $ex = explode(' ', $n);
                            echo @$ex[0][0].@$ex[1][0];
                         ?>
                    </span>
                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="<?=base_url('template/');?>img/admin-avatar.png" />
                            <span></span><?= $user->getUsers($user->id_user)->row()->nama;?><i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="<?=site_url('back/profile')?>"><i class="fa fa-user"></i>Profile</a>
                            <a class="dropdown-item" href="<?=site_url('front/login/logout')?>"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>