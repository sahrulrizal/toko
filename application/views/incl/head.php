<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?=$title;?></title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="<?=base_url('template/');?>vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url('template/');?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?=base_url('template/');?>vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
     <link href="<?=base_url('template/');?>vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?=base_url('template/');?>vendors/DataTables/datatables.min.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="<?=base_url('template/');?>css/main.min.css" rel="stylesheet" />
     <link href="<?=base_url('template/');?>css/easy-autocomplete.min.css" rel="stylesheet" />
     <link href="<?=base_url('template/');?>css/easy-autocomplete.themes.min.css" rel="stylesheet" />
     <link href="<?=base_url('template/');?>plugin/sweetalert2/sweetalert2.min.css" rel="stylesheet" />
     <?php $this->load->view('incl/link'); ?>
    <!-- PAGE LEVEL STYLES-->
</head>
