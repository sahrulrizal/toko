 <!-- CORE PLUGINS-->
 <script src="<?=base_url('template/');?>vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
 <!-- PAGE LEVEL PLUGINS-->
   <script src="<?=base_url('template/');?>vendors/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/chart.js/dist/Chart.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/jvectormap/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>vendors/jvectormap/jquery-jvectormap-us-aea-en.js" type="text/javascript"></script>
<!-- DATATABLE -->
 <script src="<?=base_url('template/');?>vendors/DataTables/datatables.min.js" type="text/javascript"></script>
 <!-- CORE SCRIPTS-->
 <script src="<?=base_url('template/');?>js/app.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>plugin/sweetalert2/sweetalert2.min.js" type="text/javascript"></script>
 
 <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
 <script src="<?=base_url('template/');?>js/jquery.add-input-area.min.js" type="text/javascript"></script>
 <script src="<?=base_url('template/');?>js/jquery.number.js" type="text/javascript"></script>
 <!-- PAGE LEVEL SCRIPTS-->

