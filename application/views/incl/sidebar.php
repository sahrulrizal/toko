 <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="<?=base_url('template/');?>img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?= $user->getUsers($user->id_user)->row()->nama;?></div><small><?= $user->cekStatus($this->session->userdata('level'));?></small></div>
                </div>
                <ul class="side-menu metismenu">
                    <!-- <li>
                        <a href="index.html"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li> -->
                    <li class="heading">MENU</li>

                    <!-- Menu Dashboard -->
                    <?php if ($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3): ?>
                   
                    <li <?=$this->uri->segment(2) == 'dashboard' ? 'class="active"' : ''; ?>>
                        <a href="<?=site_url('back/dashboard');?>"><i class="sidebar-item-icon fa fa-tachometer "></i>
                            <span class="nav-label">Dashboard</span>
                        </a>
                    </li>

                    <?php endif ?>
                    <!-- Penutup Menu Dashboard -->
                    
                    <!-- Menu Profile -->
                    <li <?=$this->uri->segment(2) == 'profile' ? 'class="active"' : ''; ?>>
                        <a href="<?=site_url('back/profile');?>"><i class="sidebar-item-icon fa fa-user-circle"></i>
                            <span class="nav-label">Profile</span>
                        </a>
                    </li>
                    <!-- Penutup Profile -->

                    <!-- Menu Laporan -->
                     <?php if ($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3): ?>

                       <li <?=$this->uri->segment(2) == 'laporan' ? 'class="active"' : ''; ?> >
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-file-text"></i>
                            <span class="nav-label">Laporan</span><i class="fa fa-angle-left arrow"></i></a>
                            <ul class="nav-2-level collapse">
                                <li>
                                    <a <?=$this->uri->segment(3) == 'penjualan' ? 'class="active"' : ''; ?> href="<?=site_url('back/laporan/penjualan');?>">Penjualan</a>
                                </li>
                                <li>
                                    <a <?=$this->uri->segment(3) == 'pembelian' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/pembelian');?>">Pembelian</a>
                                </li>
                                <li>
                                    <a <?=$this->uri->segment(3) == 'pengeluaran' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/pengeluaran');?>">Pengeluaran</a>
                                </li>
                                <li>
                                    <a <?=$this->uri->segment(3) == 'retur' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/retur');?>">Retur</a>
                                </li>
                                <li>
                                    <a <?=$this->uri->segment(3) == 'pulsaDanLagu' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/pulsaDanLagu');?>">Pulsa Dan Lagu</a>
                                </li>
                                <!-- <li>
                                    <a <?=$this->uri->segment(3) == 'pulsaTransfer' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/pulsaTransfer');?>">Pulsa Transfer</a>
                                </li> -->
                                <li>
                                    <a <?=$this->uri->segment(3) == 'pulsaDeposit' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/pulsaDeposit');?>">Pulsa Deposit</a>
                                </li>
                                <li>
                                    <a <?=$this->uri->segment(3) == 'paketIsiUlang' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/paketIsiUlang');?>">Paket Isi Uang</a>
                                </li>
                                <li>
                                    <a <?=$this->uri->segment(3) == 'diskon' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/diskon');?>">Diskon</a>
                                </li>
                                 <li>
                                    <a <?=$this->uri->segment(3) == 'service' ? 'class="active"' : ''; ?>  href="<?=site_url('back/laporan/service');?>">Service</a>
                                </li>
                            </ul>
                        </li>
                        <li <?=$this->uri->segment(2) == 'manageToko' ? 'class="active"' : ''; ?>>
                            <a href="<?=site_url('back/manageToko');?>"><i class="sidebar-item-icon fa fa-cube"></i>
                                <span class="nav-label">Manage Toko</span>
                            </a>
                        </li>
                     <?php endif ?>
                     <!-- Penutup Menu Laporan -->

                    <!-- Menu Data Barang -->
                    <?php if ($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3): ?>
                        
                    <li <?=$this->uri->segment(2) == 'barang' ? 'class="active"' : ''; ?>>
                        <a href="<?=site_url('back/barang');?>"><i class="sidebar-item-icon fa fa-cube"></i>
                            <span class="nav-label">Data Barang</span>
                        </a>
                    </li>

                    <?php endif ?>
                    <!-- Penutup Menu Data Barang -->

                     <?php if ($this->session->userdata('level') == 1 || $this->session->userdata('level') == 3): ?>

                     <li <?=$this->uri->segment(2) == 'BarangOutlet' ? 'class="active"' : ''; ?>>
                        <a  href="<?=site_url('back/BarangOutlet');?>"><i class="sidebar-item-icon fa fa-cube"></i>
                            <span class="nav-label">Data Barang Outlet</span>
                        </a>
                    </li>
                    <li <?=$this->uri->segment(2) == 'transaksiBarang' ? 'class="active"' : ''; ?> >
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-file-text"></i>
                            <span class="nav-label">Transaksi Barang</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            <li>
                                <a <?=$this->uri->segment(3) == 'penjualan' ? 'class="active"' : ''; ?> href="<?=site_url('back/transaksiBarang/penjualan');?>">Penjualan</a>
                            </li>
                            <li>
                                <a <?=$this->uri->segment(3) == 'pembelian' ? 'class="active"' : ''; ?>  href="<?=site_url('back/transaksiBarang/pembelian');?>">Pembelian</a>
                            </li>
                            <li>
                                <a <?=$this->uri->segment(3) == 'retur' ? 'class="active"' : ''; ?>  href="<?=site_url('back/transaksiBarang/retur');?>">Retur</a>
                            </li>
                        </ul>
                    </li>

                    <?php endif ?>
                     
                     <?php if ($this->session->userdata('level') == 2 || $this->session->userdata('level') == 3): ?>

                    <li <?=$this->uri->segment(2) == 'pulsa_paket' ? 'class="active"' : ''; ?>>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-volume-control-phone"></i>
                            <span class="nav-label">Data Pulsa & Paket</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            <li>
                                <a <?=$this->uri->segment(3) == 'pulsaTransferDanLagu' ? 'class="active"' : ''; ?> href="<?=site_url('back/pulsa_paket/pulsaTransferDanLagu');?>">Pulsa Transfer & Lagu</a>
                            </li>
                            <li>
                                <a <?=$this->uri->segment(3) == 'deposit' ? 'class="active"' : ''; ?> href="<?=site_url('back/pulsa_paket/deposit');?>">Pulsa Deposit</a>
                            </li>
                             <li>
                                <a <?=$this->uri->segment(3) == 'depositBulk' ? 'class="active"' : ''; ?> href="<?=site_url('back/pulsa_paket/depositBulk');?>">Pulsa Deposit Bulk</a>
                            </li>
                            <li>
                                <a <?=$this->uri->segment(3) == 'depositpaketIsiUlangDeposit1Bulk' ? 'class="active"' : ''; ?> href="<?=site_url('back/pulsa_paket/paketIsiUlangDeposit1');?>">Paket Isi Ulang Deposit 1</a>
                            </li>

                            <li>
                                <a <?=$this->uri->segment(3) == 'paketIsiUlangDeposit2' ? 'class="active"' : ''; ?>  href="<?=site_url('back/pulsa_paket/paketIsiUlangDeposit2');?>">Paket Isi Ulang Deposit 2</a>
                            </li>

                            <li>
                                <a <?=$this->uri->segment(3) == 'paketIsiUlangMatauli' ? 'class="active"' : ''; ?> href="<?=site_url('back/pulsa_paket/paketIsiUlangMatauli');?>">Paket Isi Ulang Matauli</a>
                            </li>
                        </ul>
                    </li>
                     
                    <?php endif ?>

                    <?php if ($this->session->userdata('level') == 1 || $this->session->userdata('level') == 3): ?>
                    <li <?=$this->uri->segment(2) == 'transaksiPulsa_paket' ? 'class="active"' : ''; ?>>
                        <a href="javascript:;"><i class="sidebar-item-icon fa fa-file-text"></i>
                            <span class="nav-label">Transaksi Pulsa & Paket</span><i class="fa fa-angle-left arrow"></i></a>
                        <ul class="nav-2-level collapse">
                            <li>
                                <a <?=$this->uri->segment(3) == 'pulsaTransferDanLagu' ? 'class="active"' : ''; ?> href="<?=site_url('back/transaksiPulsa_paket/pulsaTransferDanLagu');?>">Pulsa Transfer & Lagu</a>
                            </li>
                            <li>
                                <a  <?=$this->uri->segment(3) == 'pulsaDeposit' ? 'class="active"' : ''; ?> href="<?=site_url('back/transaksiPulsa_paket/pulsaDeposit/');?>">Pulsa Deposit</a>
                            </li>
                            <li>
                                <a <?=$this->uri->segment(3) == 'paketIsiUlang' ? 'class="active"' : ''; ?> href="<?=site_url('back/transaksiPulsa_paket/paketIsiUlang/');?>">Paket Isi Ulang</a>
                            </li>
                        </ul>
                    </li>
                     <li <?=$this->uri->segment(2) == 'transaksiService' ? 'class="active"' : ''; ?>>
                        <a href="<?=site_url('back/transaksiService');?>"><i class="sidebar-item-icon fa fa-wrench"></i>
                            <span class="nav-label">Transaksi Service</span>
                        </a>
                    </li>
                     <li <?=$this->uri->segment(2) == 'transaksiPengeluaran' ? 'class="active"' : ''; ?>>
                        <a href="<?=site_url('back/transaksiPengeluaran');?>"><i class="sidebar-item-icon fa fa-money"></i>
                            <span class="nav-label">Transaksi Pengeluaran</span>
                        </a>
                    </li>
                     <?php endif ?>
                </ul>
            </div>
        </nav>